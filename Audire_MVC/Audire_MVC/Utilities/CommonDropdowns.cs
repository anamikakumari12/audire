﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Audire_EF;
using Audire_POCO;

namespace Audire_MVC.Utilities
{
    public class CommonDropdowns
    {
        Audire_dbEntities datacontext = new Audire_dbEntities();
        public static List<SelectListItem> GetModuleList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<ModulePOCO> listModule = new List<ModulePOCO>();
            try
            {
                listModule = (List<ModulePOCO>)HttpContext.Current.Session["ddlModule"];
                list = listModule.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.module_id),
                    Text = c.module
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static List<SelectListItem> GetModuleListAsPerUser()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<ModulePOCO> listModule = new List<ModulePOCO>();
            try
            {
                string accessedModule = Convert.ToString(HttpContext.Current.Session["ModuleAccessed"]);
                List<int> ModuleIds = accessedModule.Split(',').Select(int.Parse).ToList();
                listModule = (List<ModulePOCO>)HttpContext.Current.Session["ddlModule"];
                list = listModule.AsEnumerable().Where(c => ModuleIds.Contains(c.module_id)).Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.module_id),
                    Text = c.module
                }).ToList();
                if(String.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["module"])))
                    HttpContext.Current.Session["module"] = list[0].Value;
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static List<SelectListItem> GetModuleListAsPerUserForQR()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<ModulePOCO> listModule = new List<ModulePOCO>();
            try
            {
                string accessedModule = Convert.ToString(HttpContext.Current.Session["ModuleAccessed"]);
                List<int> ModuleIds = accessedModule.Split(',').Select(int.Parse).ToList();
                listModule = (List<ModulePOCO>)HttpContext.Current.Session["ddlModule"];
                list = listModule.AsEnumerable().Where(c => ModuleIds.Contains(c.module_id) && c.module_id!=1).Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.module_id),
                    Text = c.module
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static List<Permissions> GetPermissionList()
        {
            List<Permissions> list = new List<Permissions>();
            try
            {
                list = (List<Permissions>)HttpContext.Current.Session["Permissionlist"];
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }

            return list;
        }
        

        //public static List<SelectListItem> GetFrequencyList(RolePOCO m)
        //{
        //    List<SelectListItem> list = new List<SelectListItem>();
        //    try
        //    {
        //        SelectListItem item = new SelectListItem();
        //        item.Text = "Daily";
        //        item.Value = "Daily";
        //        item.Selected = m != null && m.measurement_unit == "Daily";
        //        list.Add(item);
        //        item = new SelectListItem();
        //        item.Text = "Weekly";
        //        item.Value = "Weekly";
        //        item.Selected = m != null && m.measurement_unit == "Weekly";
        //        list.Add(item);
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonFunctions.ErrorLog(ex);
        //    }

        //    return list;
        //}

        public static List<SelectListItem> GetTimeZones(LocationPOCO m)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                var timeZoneList = TimeZoneInfo
                .GetSystemTimeZones()
                .Select(t => new SelectListItem
                {
                    Text = t.DisplayName,
                    Value = t.Id,
                    Selected = m != null && t.Id == m.TimeZoneId
                });
                list = timeZoneList.ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return list;
        }

        public static IEnumerable<SelectListItem> GetShifts(LocationPOCO m)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            try
            {
                SelectListItem l = new SelectListItem();
                l.Text = "1";
                l.Value = "1";
                if (l.Value == Convert.ToString(m.shifts))
                    l.Selected = true;
                list.Add(l);
                l = new SelectListItem();
                l.Text = "2";
                l.Value = "2";
                if (l.Value == Convert.ToString(m.shifts))
                    l.Selected = true;
                list.Add(l);

                l = new SelectListItem();
                l.Text = "3";
                l.Value = "3";
                if (l.Value == Convert.ToString(m.shifts))
                    l.Selected = true;
                list.Add(l);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return list;
        }

        public static IEnumerable<SelectListItem> GetLocationList(UsersPOCO m)
        {

            List<SelectListItem> list = new List<SelectListItem>();
            List<LocationPOCO> listLocation = new List<LocationPOCO>();
            ddlLocationPOCO objLoc = new ddlLocationPOCO();
            try
            {
                if (HttpContext.Current.Session["objLoginLoc"] != null && HttpContext.Current.Session["objLoginLoc"] != "")
                    objLoc = (ddlLocationPOCO)HttpContext.Current.Session["objLoginLoc"];
                listLocation = (List<LocationPOCO>)HttpContext.Current.Session["ddlLocation"];
                if (m != null && m.location_id !=null && m.location_id!=0)
                {
                    list = listLocation.AsEnumerable().Select(c => new SelectListItem
                    {
                        Value = Convert.ToString(c.location_id),
                        Text = c.location,
                        Selected = m != null && m.location_id == c.location_id
                    }).ToList();
                }
                else
                {
                    list = listLocation.AsEnumerable().Select(c => new SelectListItem
                    {
                        Value = Convert.ToString(c.location_id),
                        Text = c.location,
                        Selected =c.location_id == objLoc.location_id
                    }).ToList();
                }
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static IEnumerable<SelectListItem> GetRole(UsersPOCO m)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<RolePOCO> listRole = new List<RolePOCO>();
            try
            {
                listRole = (List<RolePOCO>)HttpContext.Current.Session["listRole"];
                list = listRole.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.role_id),
                    Text = c.role_name
                    //Selected = m != null && m.Role == c.role_desc
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }



        public static IEnumerable<SelectListItem> GetManager(int location_id)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<UsersPOCO> listManager = new List<UsersPOCO>();
            ddlLocationPOCO objLoc = new ddlLocationPOCO();
            try
            {
                if(location_id==0)
                {
                    if (HttpContext.Current.Session["objLoginLoc"] != null && HttpContext.Current.Session["objLoginLoc"] != "")
                    {
                        objLoc = (ddlLocationPOCO)HttpContext.Current.Session["objLoginLoc"];
                        location_id = objLoc.location_id;
                    }
                }
                listManager = (List<UsersPOCO>)HttpContext.Current.Session["listUsers"];
               
                list = listManager.AsEnumerable()
                    .Where(c=>c.location_id==Convert.ToInt32(location_id))
                    .Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.user_id),
                    Text = c.FullName
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }




        public static IEnumerable<SelectListItem> GetRegionList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<RegionPOCO> listRegion = new List<RegionPOCO>();
            ddlLocationPOCO objLoc = new ddlLocationPOCO();
            try
            {
                if (HttpContext.Current.Session["objLoginLoc"] != null && HttpContext.Current.Session["objLoginLoc"] != "")
                    objLoc = (ddlLocationPOCO)HttpContext.Current.Session["objLoginLoc"];
                listRegion = (List<RegionPOCO>)HttpContext.Current.Session["ddlRegion"];
                list = listRegion.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.region_id),
                    Text = c.region_name,
                    Selected = c.region_id == objLoc.region_id
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static IEnumerable<SelectListItem> GetCountryList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<CountryPOCO> listCountry = new List<CountryPOCO>();
            ddlLocationPOCO objLoc = new ddlLocationPOCO();
            try
            {
                if (HttpContext.Current.Session["objLoginLoc"] != null && HttpContext.Current.Session["objLoginLoc"] != "")
                    objLoc = (ddlLocationPOCO)HttpContext.Current.Session["objLoginLoc"];
                listCountry = (List<CountryPOCO>)HttpContext.Current.Session["ddlCountry"];
                list = listCountry.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.country_id),
                    Text = c.country_name,
                    Selected = c.country_id == objLoc.country_id
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static IEnumerable<SelectListItem> GetLineList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<LinePOCO> listLine = new List<LinePOCO>();
            try
            {
                listLine = (List<LinePOCO>)HttpContext.Current.Session["ddlLine"];
                list = listLine.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.line_id),
                    Text = c.line_name
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public IEnumerable<SelectListItem> GetAudit_Type()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<PerformAuditPOCO> listAuditType = new List<PerformAuditPOCO>();
            try
            {
                listAuditType = datacontext.tbl_audit_type.AsEnumerable().Select(m => new PerformAuditPOCO
                {
                    ID = m.ID,
                    audit_type = m.Audit_Type
                }).ToList();
                //listLine = (List<PerformAuditPOCO>)HttpContext.Current.Session["ddlAudit_Type"];
                list = listAuditType.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.ID),
                    Text = c.audit_type
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static IEnumerable<SelectListItem> GetSectionList()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<QuestionsPOCO> listSection = new List<QuestionsPOCO>();
            try
            {
                listSection = (List<QuestionsPOCO>)HttpContext.Current.Session["ddlSection"];
                list = listSection.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.section_id),
                    Text = c.section_name
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static IEnumerable<SelectListItem> getProcessList(PerformAuditPOCO m)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<SelectListItem> list1 = new List<SelectListItem>();
            List<PerformAuditPOCO> listProcess = new List<PerformAuditPOCO>();
            try
            {              
                listProcess = (List<PerformAuditPOCO>)HttpContext.Current.Session["ddlProcess"];
                SelectListItem item = new SelectListItem();
                item.Text = "Select Assigned To";
                item.Value = "0";
                if (string.IsNullOrEmpty(m.assignedTo))
                {
                    item.Selected = true;
                }
                list.Add(item);
                list1 = listProcess.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.id),
                    Text = c.process,
                    Selected = (m.assignedTo == Convert.ToString(c.id)) ? true : false
                }).ToList();
                list.AddRange(list1);
                
                return list.Distinct().ToList();
               
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }

        public static string GetUsername()
        {

            return Convert.ToString(HttpContext.Current.Session["LoginUsername"]);
        }

        public static string GetPassword()
        {

            return Convert.ToString(HttpContext.Current.Session["Password"]);
        }
        public static string GetUserid()
        {

            return Convert.ToString(HttpContext.Current.Session["LoginID"]);
        }

        public static IEnumerable<SelectListItem> getProcessDropdown()
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<ProcesssPOCO> listProcess = new List<ProcesssPOCO>();
            try
            {
                listProcess = (List<ProcesssPOCO>)HttpContext.Current.Session["ddlProcess"];
                list = listProcess.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.id),
                    Text = c.Process
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }


        public IEnumerable<SelectListItem> GetUserList()
        {

            List<SelectListItem> list = new List<SelectListItem>();
            List<UsersPOCO> listUser = new List<UsersPOCO>();
            try
            {
                listUser = datacontext.tbl_user_master.AsEnumerable().Select(m => new UsersPOCO
                {
                    user_id = m.user_id,
                    FullName = m.emp_full_name
                }).ToList();

                list = listUser.AsEnumerable().Select(c => new SelectListItem
                {
                    Value = Convert.ToString(c.user_id),
                    Text = c.FullName
                }).ToList();
                return list.Distinct().ToList();
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return list;
            }
        }


        public void SetSessionForMobileUser(string userId)
        {
            List<tbl_user_master> obj = new List<tbl_user_master>();
            int user = Convert.ToInt32(userId);
            try
            {
                var result1 = datacontext.tbl_user_master.
           Where(
               m => m.user_id == user).
               AsEnumerable().ToList();

                obj.AddRange(result1);

                foreach (var result in obj)
                {
                    if (result.end_date.Value > DateTime.Now || result.end_date.Value == null)
                    {
                        var authTicket = new FormsAuthenticationTicket(
                            1, result.emp_full_name, DateTime.Now,
                            DateTime.Now.AddHours(2), false,
                            "Admin",
                            "/"
                            );
                        HttpContext.Current.Response.Cookies.Clear();
                        var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                        HttpContext.Current.Response.Cookies.Add(cookie);
                        HttpContext.Current.Session["LoginFullname"] = result.emp_full_name;
                        HttpContext.Current.Session["LoginUsername"] = result.username;
                        HttpContext.Current.Session["EmailId"] = result.email_id;
                        HttpContext.Current.Session["LoginID"] = result.user_id;
                        HttpContext.Current.Session["global_admin"] = result.global_admin_flag;
                        HttpContext.Current.Session["ModuleAccessed"] = result.module_id;

                    }
                }
                var res = datacontext.tbl_Module_master.Where(m => m.status == 1).
                        AsEnumerable().Select(c => new ModulePOCO
                        {
                            module = c.module,
                            module_id = c.module_id
                        }).ToList();
                HttpContext.Current.Session["ddlModule"] = res;
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }

        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Audire_POCO;

namespace Audire_MVC.Utilities
{
    public static class CommonFunctions
    {
        public static string CreateRandomPassword()
        {
            int passwordLength = 8;
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[passwordLength];
            Random rd = new Random();

            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        public static string SHA1Passwordencryption(string password)
        {
            //create new instance of md5
            MD5 md5 = MD5.Create();

            //convert the input text to array of bytes
            byte[] hashData = md5.ComputeHash(Encoding.Default.GetBytes(password));

            //create new instance of StringBuilder to save hashed data
            StringBuilder returnValue = new StringBuilder();

            //loop for each byte and add it to StringBuilder
            for (int i = 0; i < hashData.Length; i++)
            {
                returnValue.Append(hashData[i].ToString());
            }

            // return hexadecimal string
            return returnValue.ToString();
        }

        public static string Passwordencryption(string password)
        {
            string returnValue = string.Empty;
            try
            {
                string EncryptionKey = "KNS_Audire";
                byte[] clearBytes = Encoding.Unicode.GetBytes(password);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(clearBytes, 0, clearBytes.Length);
                            cs.Close();
                        }
                        returnValue = Convert.ToBase64String(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return returnValue;
        }

        public static string MD5Encrypt(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            StringBuilder strBuilder = new StringBuilder();
            try
            {
                //compute hash from the bytes of text  
                md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));

                //get hash result after compute it  
                byte[] result = md5.Hash;


                for (int i = 0; i < result.Length; i++)
                {
                    //change it into 2 hexadecimal digits  
                    //for each byte  
                    strBuilder.Append(result[i].ToString("x2"));
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return strBuilder.ToString();
        }

        internal static void ErrorLog(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            DateTime dateTime = DateTime.UtcNow.Date;
            string ErrorLogFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorLogFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";
            string ErrorFile = Convert.ToString(ConfigurationManager.AppSettings["ErrorFile"]) + "_" + dateTime.ToString("dd_MM_yyyy") + ".txt";

            //string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            //string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        internal static void TestLog(string test)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", test);
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;
            string ErrorLogFile = ConfigurationManager.AppSettings["ErrorLogFile"].ToString();
            string ErrorFile = ConfigurationManager.AppSettings["ErrorFile"].ToString();
            if (!File.Exists(ErrorFile))
            {
                log = new StreamWriter(ErrorLogFile, true);
            }
            else
            {
                log = File.AppendText(ErrorLogFile);
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            // Close the stream:
            log.Close();
        }

        public static string Decrypt(string cipherText)
        {
            try
            {
                //string EncryptionKey = "MAKV2SPBNI99212";
                string EncryptionKey = "KNS_Audire";
                byte[] cipherBytes = Convert.FromBase64String(cipherText);
                using (Aes encryptor = Aes.Create())
                {
                    Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                    encryptor.Key = pdb.GetBytes(32);
                    encryptor.IV = pdb.GetBytes(16);
                    encryptor.Padding = PaddingMode.PKCS7;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateDecryptor(), CryptoStreamMode.Write))
                        {
                            cs.Write(cipherBytes, 0, cipherBytes.Length);
                            cs.Close();
                        }
                        cipherText = Encoding.Unicode.GetString(ms.ToArray());
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return cipherText;
        }
    }
}
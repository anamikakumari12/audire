﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Web;
using Audire_EF;
using Audire_POCO;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Configuration;
using Audire_MVC.Utilities;
using System.Data.Objects;
//using Amazon.SimpleNotificationService;
//using Amazon.SimpleNotificationService.Model;

namespace Audire_MVC.Models
{
    public class MastersModel
    {
        Audire_dbEntities datacontext = new Audire_dbEntities();
        
        public List<RolePOCO> getRoleList()
        {
            List<RolePOCO> listRole = new List<RolePOCO>();
            listRole = (from m in datacontext.sp_getRoleDetails()
                        select new RolePOCO {
                            role_id=Convert.ToInt32(m.Role_id),
                            users=Convert.ToString(m.users),
                            permissions=Convert.ToString(m.Role_Access),
                            role_name=Convert.ToString(m.Role_name)
            }).ToList();

            List<int> TagIds;
            List<Permissions> perList;
            Permissions per;
            foreach (RolePOCO obj in listRole)
            {
                perList = new List<Permissions>();
                TagIds = obj.permissions.Split(',').Select(int.Parse).ToList();
                for(int i=0;i<TagIds.Count;i++)
                {
                    per = new Permissions();
                    per.ID = TagIds[i];
                    perList.Add(per);
                }

                obj.htmlusers =  new HtmlString(string.IsNullOrEmpty(obj.users)?"":(obj.users.Replace("&lt;", "<").Replace("&gt;", ">")));
                obj.permission = perList;
            }
            //listRole = datacontext.tbl_role_master.Where(m => m.end_date > DateTime.Now ||
            //                                                    m.end_date == null
            //    ).
            //        AsEnumerable().OrderBy(m=>m.rolename)
            //        .Select(c => new RolePOCO
            //        {
            //            role_id = c.role_id,
            //            role_desc = c.rolename,
            //            audit_no = Convert.ToInt32(c.no_of_audits_required),
            //            measurement_unit = c.frequency
            //        }).ToList();
            return listRole;
        }

        internal List<AuditStatus> getConsolidatedAuditStatus(int location_id, int module, int user_id)
        {
            List<AuditStatus> listAstatus = new List<AuditStatus>();

            listAstatus = datacontext.sp_getConsolidatedAuditStatus(location_id,module,user_id).AsEnumerable().Select(c => new AuditStatus
            {
               module=c.module,
               audit_id=Convert.ToString(c.audit_id),
               audit_performed_date=c.audit_performed_date,
               Audit_number=c.Audit_number,
               Audit_Type=c.Audit_Type,
               emp_full_name=c.emp_full_name,
               status=c.status

            }).ToList();

            return listAstatus;
        }

        internal List<Permissions> getPermissions()
        {
            List<Permissions> list = new List<Permissions>();

            list = datacontext.tbl_permissions.
                    AsEnumerable().OrderBy(m => m.ID)
                    .Select(c => new Permissions
                    {
                        ID = c.ID,
                        Permission = c.Permission
                    }).ToList();
            return list;
        }

        internal List<LocationPOCO> getLocationList()
        {
            List<LocationPOCO> listloc = new List<LocationPOCO>();
            listloc = (from loc in datacontext.tbl_location_master
                       join country in datacontext.tbl_country_master on loc.country_id equals country.country_id
                       join reg in datacontext.tbl_region_master on loc.region_id equals reg.region_id
                       orderby loc.location_id
                       select new LocationPOCO
                      {
                          region = reg.region_name,
                          region_id = reg.region_id,
                          country_id = country.country_id,
                          country = country.country_name,
                          location_id = loc.location_id,
                          location = loc.location_name,
                          shifts = (int)loc.no_of_shifts,
                          starttime1 = (TimeSpan)loc.shift1_start_time,
                          endtime1 = (TimeSpan)loc.shift1_end_time,
                          starttime2 = (TimeSpan)loc.shift2_start_time,
                          endtime2 = (TimeSpan)loc.shift2_end_time,
                          starttime3 = (TimeSpan)loc.shift3_start_time,
                          endtime3 = (TimeSpan)loc.shift3_end_time,
                          TimeZoneId = loc.timezone_code,
                          timezone = loc.timezone_desc
                      }).ToList();

            return listloc;
        }

       

        public int getRegionId(string region = null, int location_id = 0)
        {
            int region_id = 0;
            try
            {
                if (!string.IsNullOrEmpty(region))
                {
                    var regions = (from m in datacontext.tbl_region_master
                                   where m.region_name == region
                                   select new
                                   {
                                       m.region_id
                                   }).ToList();

                    if (regions.Count > 0)
                        region_id = Convert.ToInt32(regions[0].region_id);
                }
                else if (location_id > 0)
                {
                    var regions = (from m in datacontext.tbl_location_master
                                   where m.location_id == location_id
                                   select new
                                   {
                                       m.region_id
                                   }
                              ).ToList();

                    if (regions.Count > 0)
                        region_id = Convert.ToInt32(regions[0].region_id);

                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return region_id;
        }

        
        public int getCountryId(string country = null, int location_id = 0)
        {
            int country_id = 0;
            try
            {
                if (!string.IsNullOrEmpty(country))
                {
                    var countries = (from m in datacontext.tbl_country_master
                                     where m.country_name == country
                                     select new
                                     {
                                         m.country_id
                                     }
                           ).ToList();

                    if (countries.Count > 0)
                        country_id = Convert.ToInt32(countries[0].country_id);
                }
                else if (location_id > 0)
                {
                    var countries = (from m in datacontext.tbl_location_master
                                     where m.location_id == location_id
                                     select new
                                     {
                                         m.country_id
                                     }
                              ).ToList();

                    if (countries.Count > 0)
                        country_id = Convert.ToInt32(countries[0].country_id);

                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return country_id;
        }

       

        public int getLocationId(string location, int country_id)
        {
            int location_id = 0;
            try
            {
                if (!string.IsNullOrEmpty(location))
                {
                    var countries = (from m in datacontext.tbl_location_master
                                     where m.country_id == country_id &&
                                     m.location_name==location
                                     select new
                                     {
                                         m.location_id
                                     }
                           ).ToList();

                    if (countries.Count > 0)
                        location_id = Convert.ToInt32(countries[0].location_id);
                }
               
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return location_id;
        }

        //internal List<ProcesssPOCO> getProcessList()
        //{
        //    List<ProcesssPOCO> Process_name = new List<ProcesssPOCO>();
        //    Process_name =
        //            datacontext.tbl_process_master.AsEnumerable().Select(m => new ProcesssPOCO
        //            {
        //                id = m.id,
        //                Process = m.Process,
        //            }).ToList();
        //    //Process_name = (from c in datacontext.tbl_user_master
        //    //                select new ProcesssPOCO
        //    //           {
        //    //               emp_full_name = c.emp_full_name,
        //    //           }).ToList();
        //    return Process_name;
        //}

        public tbl_user_master ResetPassword(tbl_user_master user)
        {
            tbl_user_master userLogintbl = new tbl_user_master();
            try
            {
                userLogintbl = datacontext.tbl_user_master.AsQueryable().
                    FirstOrDefault(k => k.user_id == user.user_id);

                if (userLogintbl != null)
                {
                    //userLogintbl.passwd = user.passwd;
                    userLogintbl.passwd = Audire_MVC.Utilities.CommonFunctions.Passwordencryption(user.passwd);
                    datacontext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return userLogintbl;
        }

        public List<tbl_user_master> ValidateUser(tbl_user_master login_employee)
        {
            List<tbl_user_master> obj = new List<tbl_user_master>();
            try
            {
                var result = datacontext.tbl_user_master.
           Where(
               m => m.username == login_employee.username &&
               m.passwd == login_employee.passwd).
               AsEnumerable().ToList();

                obj.AddRange(result);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }

            return obj;
        }

        public tbl_user_master GetLoginUserDetail(string username)
        {
            var result =
                datacontext.tbl_user_master.FirstOrDefault(m => m.username == username || m.email_id== username);
            return result;
        }

        internal List<ProcesssPOCO> getempList()
        {
            List<ProcesssPOCO> Process_name = new List<ProcesssPOCO>();
            Process_name = datacontext.tbl_user_master.AsEnumerable().Select(m => new ProcesssPOCO
                       {
                           emp_full_name = m.emp_full_name,
                       }).ToList();
            return Process_name;
        }

        internal List<UsersPOCO> getUsersList()
        {
            List<UsersPOCO> listUsers = new List<UsersPOCO>();
            //listUsers = datacontext.Database.ExecuteSqlCommand("EXEC [dbo].[sp_getUserList]");
            listUsers = datacontext.sp_getUserList().AsEnumerable().Select(c => new UsersPOCO
            //listUsers = datacontext.tbl_user_master.Where(m => m.end_date > DateTime.Now || m.end_date == null).
            //        AsEnumerable().Select(c => new UsersPOCO
            {
                user_id = c.user_id,
                UserName = c.username,
                FirstName = c.emp_first_name,
                LastName = c.emp_last_name,
                FullName=c.emp_full_name,
                EmailId = c.email_id,
                encryptedpwd = c.passwd,
                password = Audire_MVC.Utilities.CommonFunctions.Decrypt(c.passwd),
                privacySetting = !string.IsNullOrEmpty(c.display_name_flag) && c.display_name_flag == "Y" ? true : false,
                Admin = !string.IsNullOrEmpty(c.global_admin_flag) && c.global_admin_flag == "Y" ? true : false,
                module = c.module_id,
                process = c.process_id,
                //Admintype = c.global_admin_flag,
                Role = c.role,
                location_id = c.location_id,
                location = c.location,
                startdate = c.start_date,
                enddate = c.end_date,
                //Convert.ToDateTime(c.end_date).ToString("MM-dd-yyyy", CultureInfo.InvariantCulture),
                module_id_list = c.module,
                module_list = c.module,
                process_list = c.process,
                process_id_list = c.process_id,
                reporting_manager=Convert.ToInt32(c.reporting_manager)

            }).ToList();

            //listUsers = (from c in datacontext.tbl_user_master
            //             join loc in datacontext.tbl_location_master on c.location_id equals loc.location_id
            //             orderby loc.location_name
            //             select new UsersPOCO
            //           {
            //               user_id = c.user_id,
            //               UserName = c.username,
            //               FirstName = c.emp_first_name,
            //               LastName = c.emp_last_name,
            //               EmailId = c.email_id,
            //               encryptedpwd = c.passwd,
            //               privacySetting = string.IsNullOrEmpty(c.display_name_flag) ? false : true,
            //               Admintype = c.global_admin_flag,
            //               Role = c.role,
            //               module = c.module_id,
            //               process = c.process_id,
            //               location_id = c.location_id,
            //               location = loc.location_name,
            //               startdate = c.start_date,
            //               enddate = c.end_date
            //           }).ToList();
            return listUsers;
        }

        public string getRole(int role_id = 0)
        {
            string Role = string.Empty;
            try
            {
                if (role_id > 0)
                {
                    var roles = (from m in datacontext.tbl_role_master
                                 where m.Role_id == role_id
                                 select new
                                 {
                                     m.Role_name
                                 }
                              ).ToList();

                    if (roles.Count > 0)
                        Role = Convert.ToString(roles[0].Role_name);

                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return Role;
        }

        internal void ErrorLog(Exception ex)
        {
            CommonFunctions.ErrorLog(ex);
        }

        public List<RegionPOCO> getRegionList()
        {
            List<RegionPOCO> region_name = new List<RegionPOCO>();
            try
            {
                //var region = (from m in datacontext.tbl_region_master
                //              select new { 
                //              m.region_id,
                //              m.region_name
                //              });

                region_name =
                    datacontext.tbl_region_master.AsEnumerable().Select(m => new RegionPOCO
                    {
                        region_id = m.region_id,
                        region_name = m.region_name
                    }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return region_name;
        }

        public List<PerformAuditPOCO> getProcessList()
        {
            List<PerformAuditPOCO> listProcess = new List<PerformAuditPOCO>();
            listProcess =
                    datacontext.tbl_process_master.AsEnumerable().Select(m => new PerformAuditPOCO
                    {
                        id = m.id,
                        process = m.Process,
                    }).ToList();
            return listProcess;
        }

        public List<ProcesssPOCO> ProcessList()
        {
            List<ProcesssPOCO> listProcess = new List<ProcesssPOCO>();
            listProcess =
                    datacontext.tbl_process_master.AsEnumerable().Select(m => new ProcesssPOCO
                    {
                        id = m.id,
                        Process = m.Process,
                    }).ToList();

            return listProcess;
        }

        public List<ProcesssPOCO> getProcessDropdown()
        {
            List<ProcesssPOCO> listProcess = new List<ProcesssPOCO>();
            listProcess =
                    datacontext.tbl_process_master.AsEnumerable().Select(m => new ProcesssPOCO
                    {
                        id = m.id,
                        Process = m.Process,
                    }).ToList();
            return listProcess;
        }

        public List<CountryPOCO> getCountryList()
        {
            List<CountryPOCO> country_name = new List<CountryPOCO>();
            try
            {
                country_name =
                   datacontext.tbl_country_master.AsEnumerable().Select(m => new CountryPOCO
                   {
                       country_id = m.country_id,
                       country_name = m.country_name

                   }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return country_name;
        }

        public List<LinePOCO> getLineList()
        {
            List<LinePOCO> listline = new List<LinePOCO>();
            listline = (from line in datacontext.tbl_line_master
                        join location in datacontext.tbl_location_master on line.location_id equals location.location_id
                        join country in datacontext.tbl_country_master on line.country_id equals country.country_id
                        join reg in datacontext.tbl_region_master on line.region_id equals reg.region_id

                        orderby line.line_id
                        select new LinePOCO
                         {
                             region = reg.region_name,
                             region_id = reg.region_id,
                             country_id = country.country_id,
                             country = country.country_name,
                             location_id = location.location_id,
                             location = location.location_name,
                             line_id = line.line_id,
                             line_code = line.line_code,
                             line_name = line.line_name,
                             start_date = line.start_date,
                             end_date = line.end_date,
                             distribution_list = line.distribution_list
                         }).ToList();

            return listline;
        }

        public List<PerformAuditPOCO> getAuditType()
        {
            List<PerformAuditPOCO> listAuditType = new List<PerformAuditPOCO>();
            listAuditType =
                     datacontext.tbl_audit_type.AsEnumerable().Select(m => new PerformAuditPOCO
                     {
                         ID = m.ID,
                         audit_type = m.Audit_Type
                     }).ToList();
            return listAuditType;
        }

        public List<PartPOCO> GetProduct(int p_user_id)
        {
            Nullable<int> p_id = string.IsNullOrEmpty(Convert.ToString(p_user_id)) ? 0 : Convert.ToInt32(p_user_id);
            DataTable dt = new DataTable();
            List<PartPOCO> obj = new List<PartPOCO>();
          //  obj=(from part in datacontext.sp_getProductListforEdit);
            obj = (from part in datacontext.sp_getProductListforEdit(p_id)
                   select new PartPOCO
                        {
                            region = part.region_name,
                            region_id = part.region_id,
                            country_id = part.country_id,
                            country = part.country_name,
                            location_id = part.location_id,
                            location = part.location_name,
                            line_id = part.line_id,
                            line_code = part.line_code,
                            line_name = part.line_name,
                            product_name = part.product_name,
                        }).ToList();
            return obj;
        }
        internal List<PlanRatePOCO> getPlanConsolidated(int module_id, int location_id)
        {
            DataTable dt = new DataTable();
            List<PlanRatePOCO> obj = new List<PlanRatePOCO>();
            obj = (from pln in datacontext.sp_GetPlanConsolidated(location_id, module_id)
                   select new PlanRatePOCO
                   {
                       TotalPlan=Convert.ToString(pln.TotalPlan),
                       LMTotalPlan= Convert.ToString(pln.LMTotalPlan),
                       TotalPlanRate= Convert.ToString(pln.TotalPlanRate),
                       TotalPlanCompleted= Convert.ToString(pln.TotalPlanCompleted),
                       LMTotalCompleted= Convert.ToString(pln.LMTotalCompleted),
                       TotalCompletedRate= Convert.ToString(pln.TotalCompletedRate)
                   }).ToList();
            return obj;
        }
        public List<ddlOpenTaskPOCO> GetOpenCloseTaskddl(int audit_id)
        {
            List<ddlOpenTaskPOCO> obj = new List<ddlOpenTaskPOCO>();
            obj = (from task in datacontext.sp_getdropdowndetail(audit_id)
                   select new ddlOpenTaskPOCO
                   {
                       region_id=task.region_id,
                       country_id=task.country_id,
                       location_id=task.location_id,
                       module_id=task.module_id,
                       audit_type=task.AuditType_id
                   }).ToList();
            return obj;
        }


        public List<OpenCloseTaskPOCO> GetOpenCloseTask(int p_region_id, int p_country_id, int p_location_id, int p_line_id, int module_id,int auditType_id, int p_user_id)
        {
            int? p_reg_id = string.IsNullOrEmpty(Convert.ToString(p_region_id)) ? 0 : Convert.ToInt32(p_region_id);
            int? p_con_id = string.IsNullOrEmpty(Convert.ToString(p_country_id)) ? 0 : Convert.ToInt32(p_country_id);
            int? p_loc_id = string.IsNullOrEmpty(Convert.ToString(p_location_id)) ? 0 : Convert.ToInt32(p_location_id);
            int? p_lin_id = string.IsNullOrEmpty(Convert.ToString(p_line_id)) ? 0 : Convert.ToInt32(p_line_id);
            int? p_mod_id = string.IsNullOrEmpty(Convert.ToString(module_id)) ? 0 : Convert.ToInt32(module_id);
            int? p_aud_id = string.IsNullOrEmpty(Convert.ToString(auditType_id)) ? 0 : Convert.ToInt32(auditType_id);
            int? p_usr_id = string.IsNullOrEmpty(Convert.ToString(p_user_id)) ? 0: Convert.ToInt32(p_user_id);
            List<OpenCloseTaskPOCO> obj = new List<OpenCloseTaskPOCO>();
            obj = (from task in datacontext.sp_getOpenTasks(p_reg_id, p_con_id, p_loc_id, p_lin_id, p_mod_id,p_aud_id,p_usr_id )
                   select new OpenCloseTaskPOCO
                        {
                            answer_id=task.answer_id,
                            audit_id= task.audit_id,                            
                            question_id=Convert.ToInt32(task.question_id),
                            answer= task.answer,
                            audit_comment=task.audit_comment,
                            audit_images= task.audit_images,
                            question = task.question,
                            review_comment=task.review_comment,
                            audit_number=task.Audit_number,
                            assigned_to=task.assigned_To,
                            root_cause=task.root_cause,
                            corrective_action=task.Corrective_Action,
                            preventive_action=task.Preventive_Action,
                            clause=task.clause
                        }).ToList();
            return obj;
        }

        public int SaveProduct(string p_add_edit_flag, DataTable p_parts_table)
        {
            int result = 0;
            try
            {
                var partTable = new SqlParameter("p_parts_table", SqlDbType.Structured);
                partTable.Value = p_parts_table;
                partTable.TypeName = "PartsTableType";
                var flag = new SqlParameter("p_add_edit_flag", SqlDbType.VarChar);
                flag.Value = p_add_edit_flag;
                result = datacontext.Database.ExecuteSqlCommand("EXEC [dbo].[sp_insLineProduct_bulk] @p_add_edit_flag, @p_parts_table ", flag, partTable);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return Convert.ToInt32(result);
        }

        public List<ProcesssPOCO> GetProcess()
        {
            List<ProcesssPOCO> obj = new List<ProcesssPOCO>();
            try
            {
                //obj = (from process in datacontext.sp_getProcessEmpMapping()
                //       select new ProcesssPOCO
                //       {
                //           id = process.process_id,
                //           Process = process.process,
                //           emp_full_name = process.emp_full_name,
                //       }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        public string Sendmail(string emailid, string password, string toUser, string action)
        {
            try
            {
                MailMessage email = new MailMessage();
                email.To.Add(new MailAddress(emailid));
                if (action == "forgotPassword")
                {
                    email.Subject = "Audire Password Created";//Subject for your request
                    string link = "<a href=\" " + Convert.ToString(ConfigurationManager.AppSettings["AppPath"]) + "\">login</a>";
                    email.Body = " <br/><br/>New Credentials as below: <br/> Username: " + toUser + "<br/>Password: " + password + "<br/>Click here to " + link + "<br/><br/>" + "From" + "<br/>" + "IT Team - Audire";
                }
                else if (action == "resetPassword")
                {
                    email.Subject = "Audire Password Update";//Subject for your request
                    string link = "<a href=\" " + Convert.ToString(ConfigurationManager.AppSettings["AppPath"]) + "\">login</a>";
                    email.Body = " <br/><br/>Password is updated successfully for username: " + toUser + ".<br/> If you didn't change the password, please contact administrator.<br/><br/>" + "From" + "<br/>" + "IT Team - Audire";
                }
                email.IsBodyHtml = true;
                SmtpClient smtpc = new SmtpClient();
                smtpc.Send(email);
                return "Yes";
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
                return "error";
            }


        }

        internal int getSubAuditTypeId(string subAuditType, string module_id)
        {
            int subAuditType_id = 0;
            int module = Convert.ToInt32(module_id);
            try
            {
                if (!string.IsNullOrEmpty(subAuditType))
                {
                    var subAuditTypes = (from m in datacontext.tbl_audit_type
                                   where m.Audit_Type == subAuditType &&
                                    m.module_id == module
                                   select new
                                   {
                                       m.ID
                                   }).ToList();

                    if (subAuditTypes.Count > 0)
                        subAuditType_id = Convert.ToInt32(subAuditTypes[0].ID);
                }
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return subAuditType_id;
        }

        internal int getMaxQusetionId(int subAuditType_id, int module_id)
        {
            int maxQuestion_id = 0;
            try
            {
                maxQuestion_id = datacontext.tbl_question_master.Where(u => u.Audit_type_id == subAuditType_id && u.module_id == module_id).Max(u => (int)u.question_id);
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return maxQuestion_id;
        }

        internal List<ProcesssPOCO> getProcessAuditList()
        {
            List<ProcesssPOCO> obj = new List<ProcesssPOCO>();
            try
            {
                obj = (from process in datacontext.tbl_audit_type join module in datacontext.tbl_Module_master on process.module_id equals module.module_id
                       //where process.module_id==2
                       select new ProcesssPOCO
                       {
                           id = process.ID,
                           Process = process.Audit_Type,
                           module_id=process.module_id,
                           module=module.module
                       }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal List<AuditTypePOCO> getAuditTypeScores(string Id, string p, string module)
        {
            List<AuditTypePOCO> obj = new List<AuditTypePOCO>();
            try
            {
                int module_id = Convert.ToInt32(Id);
                obj = datacontext.sp_getAuditScore(module_id, p, module).AsEnumerable().Select(c => new AuditTypePOCO
                {
                    AuditType_name = Convert.ToString(c.Audit_Type),
                    AuditType_id = c.Audit_Type_id,
                    Audit_Score = Convert.ToString(c.score),
                    Audit_date = Convert.ToString(c.last_date),
                    Audit_location=Convert.ToString(c.location)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal DashboardDetailPOCO getDashboardDetail(string Id, string module, string freq)
        {
            DashboardDetailPOCO obj = new DashboardDetailPOCO();
            try
            {
                int? module_id = String.IsNullOrEmpty(Convert.ToString(Id))?0:Convert.ToInt32(Id);

                obj = datacontext.sp_getDashboardDetail(module_id,module).AsEnumerable().Select(c => new DashboardDetailPOCO
                {
                   total_Audits=Convert.ToString(c.tot_audit),
                   Open_NC=Convert.ToString(c.open_NC),
                   Closed_NC=Convert.ToString(c.closed_NC),
                   Average_MonthlyScore=Convert.ToString(c.monthly_score)+"%",
                   Average_HalfYearlyScore= Convert.ToString(c.halfyearly_score) + "%",
                   Average_QuarterlyScore= Convert.ToString(c.quarterly_score) + "%",
                    Average_YearlyScore=Convert.ToString(c.yearly_score) + "%",
                    //relativeScore =Convert.ToString(c.relative_change),
                   PendingAudits=Convert.ToString(c.pending),
                   Updated_date=Convert.ToString(DateTime.Now)
                }).Single();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal List<AuditTasks> getAllTasks()
        {
            List<AuditTasks> obj = new List<AuditTasks>();
            try
            {
                obj = datacontext.sp_getAllTasks().AsEnumerable().Select(c => new AuditTasks
                {
                    audit_id = Convert.ToString(c.audit_id),
                    Audit_Type = Convert.ToString(c.Audit_Type),
                    module = Convert.ToString(c.module),
                    audit_performed_date = Convert.ToString(c.audit_performed_date),
                    location_name=Convert.ToString(c.location_name)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal List<DashboardChart> getDashboardChart(int module_id)
        {
            List<DashboardChart> obj = new List<DashboardChart>();
            try
            {
                obj = datacontext.sp_getDashboardGraph(module_id).AsEnumerable().Select(c => new DashboardChart
                {
                    audit_count=Convert.ToInt32(c.audit_count),
                    audit_date=Convert.ToString(c.audit_date)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal List<PendingAuditsPOCO> getPendingAudits(int user_id, string module_id)
        {
            int module = Convert.ToInt32(module_id);
            List<PendingAuditsPOCO> obj = new List<PendingAuditsPOCO>();
            try
            {
                obj = datacontext.sp_getPendingAudits(user_id,module).AsEnumerable().Select(c => new PendingAuditsPOCO
                {
                    planned_date = Convert.ToString(c.planned_date),
                    plan_week = Convert.ToString(c.week_num),
                   location_id = Convert.ToInt32(c.location_id),
                    location = Convert.ToString(c.location),
                    AuditType_id=Convert.ToInt32(c.Audit_type_id),
                    AuditType=Convert.ToString(c.Audit_Type),
                    color_code=Convert.ToInt32(c.color_code)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }
        internal List<ReportPOCO> getAuditReport(ParamPOCO param)
        {
            List<ReportPOCO> obj = new List<ReportPOCO>();
            try
            {
                obj = datacontext.sp_getAuditReport(param.module_id, param.user_id).AsEnumerable().Select(c => new ReportPOCO
                {
                    audit_id = Convert.ToInt32(c.audit_id),
                    audit_number = Convert.ToString(c.Audit_number),
                    Auditor = Convert.ToString(c.emp_full_name),
                    Audit_date = Convert.ToString(c.audit_performed_date),
                    Location = Convert.ToString(c.location_name)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }
        internal List<CalendarEvent> getAuditPlans(int module_id, int location_id)
        {
            List<CalendarEvent> tasksList = new List<CalendarEvent>();
            try
            {
                tasksList = datacontext.sp_GetAuditPlan(location_id, module_id, null, null).AsEnumerable().Select(cevent=> new CalendarEvent
                    
                    {
                        audit_plan_id = cevent.audit_plan_id,
                        planned_date = Convert.ToDateTime(cevent.planned_date).ToString("MMMM yyyy"),
                        planned_day = Convert.ToDateTime(cevent.planned_date).ToString("dd"),
                    Emp_name = cevent.Emp_name,
                        line_name = cevent.line_name,
                        //allDay = false,
                        //title = Convert.ToString(cevent.Emp_name) + "(" + Convert.ToString(cevent.Audit_Type) + ")",
                        planned_end_date = Convert.ToDateTime(cevent.planned_date_end).ToString("dd/MM/yyyy"),
                        Audit_Status = Convert.ToInt32(cevent.Audit_status),
                        color_code = Convert.ToString(cevent.color_code),
                        module = Convert.ToString(cevent.module),
                        module_id = Convert.ToInt32(cevent.module_id),
                        to_be_audited_by_user_id = Convert.ToInt32(cevent.to_be_audited_by_user_id),
                        Audit_type_id = Convert.ToString(cevent.Audit_type_id),
                    AuditType_Code = Convert.ToString(cevent.AuditType_Code),
                    Audit_note=Convert.ToString(cevent.audit_note),
                    location_name=Convert.ToString(cevent.location_name)

                }).ToList();
            }


            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return tasksList;
        }
        internal List<CAPA_POCO> getCAPAReport(ParamPOCO param)
        {
            List<CAPA_POCO> obj = new List<CAPA_POCO>();
            try
            {
                obj = datacontext.sp_getCAPAReport(param.module_id, param.subAudit_id, param.user_id).AsEnumerable().Select(c => new CAPA_POCO
                {
                    audit_id = Convert.ToInt32(c.audit_id),
                    Audit_number = Convert.ToString(c.Audit_number),
                    Status = Convert.ToString(c.Final_Status),
                    target_date = Convert.ToString(c.pa_date),
                    SubAudit_Name = Convert.ToString(param.subAudit_name),
                    ID = Convert.ToInt32(param.subAudit_id),
                    confirm_comment=Convert.ToString(c.confirm_comment),
                    report_date=Convert.ToString(c.report_date),
                    containment=Convert.ToString(c.Containment)
                }).ToList();
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return obj;
        }

        internal List<SubAudit> getSubAuditType(ParamPOCO param)
        {
            List<SubAudit> list = new List<SubAudit>();
           // int count = 0;
            try
            {
                list = datacontext.tbl_audit_type.AsEnumerable().Where(c=> c.module_id==param.module_id).OrderBy(c=>c.display_seq).Select(c => new SubAudit {
                    ID=Convert.ToInt32(c.ID),
                    SubAudit_Name=Convert.ToString(c.Audit_Type)
                }).ToList();
                //count = list.Count();
                //for(int i=0;i<count;i++)
                //{
                //     switch (i % 4)
                //    {
                //        case  0:
                //            list[i].div_class = "table-responsive mn_green";
                //            break;
                //        case 1:
                //            list[i].div_class = "table-responsive mn_red_1";
                //            break;
                //        case 2:
                //            list[i].div_class = "table-responsive mn_blue";
                //            break;
                //        default:
                //            list[i].div_class = "table-responsive mn_orange";
                //            break;

                //    }
                //}
            }
            catch (Exception ex)
            {
                ErrorLog(ex);
            }
            return list;
        }

        internal Confirmation SaveConfirmComment(Confirmation objParam)
        {
            ObjectParameter err_code = new ObjectParameter("err_code",typeof(int));
            ObjectParameter err_msg = new ObjectParameter("err_msg", typeof(string));
            datacontext.sp_saveConfirmAudit(objParam.audit_id, objParam.confirm_comment, objParam.confirmed_by, err_code, err_msg);
            objParam.err_code = Convert.ToInt32(err_code.Value);
            objParam.err_msg = Convert.ToString(err_msg.Value);
                return objParam;
        }

    }
}
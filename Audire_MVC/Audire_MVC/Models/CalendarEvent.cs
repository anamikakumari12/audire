﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Audire_MVC.Models
{
    public class CalendarEvent
    {
        public int audit_plan_id { get; set; }
        public string planned_date { get; set; }
        public string planned_end_date { get; set; }
        public string Emp_name { get; set; }
        public bool allDay { get; set; }
        public string title { get; set; }
        public int line_product_rel_id { get; set; }
        public int line_id { get; set; }
        public int to_be_audited_by_user_id { get; set; }
        public string line_name { get; set; }
        public int Audit_Status { get; set; }
        public string module { get; set; }
        public string Audit_Type { get; set; }
        public string color_code { get; set; }
        public int module_id { get; set; }
        public string Audit_type_id { get; set; }
        public string Audit_note { get; set; }
        public string planned_day { get; set; }
        public string AuditType_Code { get; set; }
        public string location_name { get; set; }
    }

    public class PlanDetails
    {
        public int? audit_plan_id { get; set; }
        public string planned_date { get; set; }
        public string planned_end_date { get; set; }
        public int location { get; set; }
        public int module_id { get; set; }
        public int audit_type_id { get; set; }
        public int user_id { get; set; }
        public string invite_to_list { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
        public string audit_note { get; set; }
    }
}
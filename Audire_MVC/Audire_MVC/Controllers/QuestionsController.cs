﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using System.Data;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;
using Audire_MVC.Utilities;
using System.Data.Objects;
using System.Data.Entity.Infrastructure;

namespace Audire_MVC.Controllers
{
    public class QuestionsController : Controller
    {
        //
        // GET: /Questions/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        public ActionResult Questions()
        {

            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                List<QuestionsPOCO> listQuestions = new List<QuestionsPOCO>();
                return View(listQuestions);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        [HttpPost]
        public ActionResult DeleteQuestion(string module_id, string subAudit, string question_id)
        {
            OutputPOCO objOutput = new OutputPOCO();
            tbl_question_master tblQuest = new tbl_question_master();
            try
            {
                int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                int q_id = Convert.ToInt32(question_id);
                int m_id = Convert.ToInt32(module_id);
                var id = datacontext.tbl_question_master.Where(k => k.question_id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                datacontext.Entry(id).State = EntityState.Deleted;

                // tblQuest.question = question;
                //datacontext.tbl_question_master.Add(tblQuest);
                datacontext.SaveChanges();
                tblQuest = datacontext.tbl_question_master.Where(k => k.Audit_type_id == subAuditType_id && k.module_id == m_id).FirstOrDefault();
                if (tblQuest == null)
                {
                    var type_id = datacontext.tbl_audit_type.Where(k => k.ID == subAuditType_id && k.module_id == m_id).FirstOrDefault();

                    datacontext.Entry(type_id).State = EntityState.Deleted;
                    try
                    {
                        datacontext.SaveChanges();
                    }
                    catch (OptimisticConcurrencyException)
                    {
                        ((IObjectContextAdapter)datacontext)
        .ObjectContext
        .Refresh(RefreshMode.StoreWins, type_id);
                        datacontext.SaveChanges();
                    }
                    objOutput.err_code = 250;
                    objOutput.err_msg = "Question and product deleted successfully.";
                }
                else
                {
                    //TempData["questionmsg"] = "Question details deleted successfully.";
                    objOutput.err_code = 200;
                    objOutput.err_msg = "Question deleted successfully.";
                }

            }
            catch (Exception ex)
            {
                objOutput.err_code = 202;
                objOutput.err_msg = ex.Message;
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveQuestion(string module_id, string subAudit, string question, string parts, string question_id)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            OutputPOCO objOutput = new OutputPOCO();
            try
            {
                tbl_question_master tblQuest = new tbl_question_master();
                if (!String.IsNullOrEmpty(question_id))
                {
                    if (Convert.ToInt32(question_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        int q_id = Convert.ToInt32(question_id);
                        int m_id = Convert.ToInt32(module_id);
                        tblQuest = datacontext.tbl_question_master.AsQueryable().
                        FirstOrDefault(k => k.question_id == q_id && k.Audit_type_id == subAuditType_id && k.module_id == m_id);
                        if (tblQuest != null)
                        {
                            tblQuest.question = question;
                            //datacontext.tbl_question_master.Add(tblQuest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question details updated successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question updated successfully.";
                        }
                    }
                }
                else
                {
                    if (Convert.ToInt32(module_id) > 0)
                    {
                        int subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        if (subAuditType_id == 0)
                        {
                            tbl_audit_type objtype = new tbl_audit_type();
                            objtype.module_id = Convert.ToInt32(module_id);
                            objtype.Audit_Type = subAudit;
                            if (Convert.ToInt32(module_id) == 3)
                            {
                                objtype.Parts = parts;
                            }
                            datacontext.tbl_audit_type.Add(objtype);
                            datacontext.SaveChanges();
                            subAuditType_id = objModel.getSubAuditTypeId(subAuditType: subAudit, module_id: module_id);
                        }
                        else if (Convert.ToInt32(module_id) == 3)
                        {
                            int mod_id = Convert.ToInt32(module_id);
                            tbl_audit_type tbltype = new tbl_audit_type();
                            tbltype = datacontext.tbl_audit_type.AsQueryable().
                                FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id);
                            if (tbltype != null)
                            {
                                tbl_audit_type tblpart = new tbl_audit_type();
                                tblpart = datacontext.tbl_audit_type.AsQueryable().
                                    FirstOrDefault(k => k.module_id == mod_id && k.ID == subAuditType_id && k.Parts == parts.Trim());

                                tbltype.Parts = parts.Trim();
                                datacontext.SaveChanges();
                                //TempData["questionmsg"] = "Part details are updated successfully.";
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "Part details are updated successfully.";
                            }
                            else
                            {
                                objOutput.audit_type_id = subAuditType_id;
                                objOutput.err_code = 200;
                                objOutput.err_msg = "No Product is found for update. Please try again.";
                                //TempData["questionmsg"] = "No Product is found for update. Please try again.";
                            }
                        }
                        if (subAuditType_id > 0)
                        {
                            int Maxquest = objModel.getMaxQusetionId(subAuditType_id, Convert.ToInt32(module_id));
                            if (Maxquest < 1000)
                            {
                                Maxquest = 1001;
                            }
                            else
                            {
                                Maxquest += 1;
                            }
                            tbl_question_master objquest = new tbl_question_master();
                            objquest.Audit_type_id = subAuditType_id;
                            objquest.module_id = Convert.ToInt32(module_id);
                            objquest.question = Convert.ToString(question).Trim();
                            objquest.question_id = Maxquest;
                            datacontext.tbl_question_master.Add(objquest);
                            datacontext.SaveChanges();
                            //TempData["questionmsg"] = "Question with audit type details added successfully.";
                            objOutput.audit_type_id = subAuditType_id;
                            objOutput.err_code = 200;
                            objOutput.err_msg = "Question with audit type details added successfully.";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objOutput.err_code = 101;
                objOutput.err_msg = "Error in updating the Question details. Please try again.";
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetQuestionList(QuestionsPOCO obj)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            try
            {
                listquestion = (from m in datacontext.sp_getQuestionList(Convert.ToInt32(obj.module_id), Convert.ToInt32(obj.Audit_type_id), 0)
                                select new QuestionsPOCO
                                {
                                    question_id = m.question_id,
                                    question = m.question,
                                    Audit_type_id = Convert.ToString(m.audit_type_id),
                                    AuditType = m.Audit_Type
                                }).ToList();

                return Json(listquestion, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
                return Json(true);
            }

        }

        public PartialViewResult Clear()
        {
            ModelState.Clear();
            return PartialView("DisplayQuestion");
        }
        public PartialViewResult EditQuestion(string viewName)
        {
            QuestionsPOCO obj = new QuestionsPOCO();
            return PartialView(viewName, obj);
        }
        public PartialViewResult DisplayQuestion(QuestionsPOCO obj)
        {
            List<QuestionsPOCO> listquestion = new List<QuestionsPOCO>();
            try
            {
                listquestion = (from m in datacontext.sp_getQuestionList(Convert.ToInt32(obj.module_id), Convert.ToInt32(obj.Audit_type_id),0)
                                select new QuestionsPOCO
                                {
                                    question_id = m.question_id,
                                    question = m.question,
                                    Audit_type_id = Convert.ToString(m.audit_type_id),
                                    AuditType = m.Audit_Type
                                }).ToList();
                
            }
            catch (Exception ex)
            {
                //TempData["questionmsg"] = "Error in updating the Question details. Please try again.";
                CommonFunctions.ErrorLog(ex);
            }
            return PartialView("DisplayQuestion", listquestion);
        }
    }
}

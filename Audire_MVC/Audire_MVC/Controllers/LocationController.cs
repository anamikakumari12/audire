﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_EF;
using Audire_MVC.Models;
using Audire_POCO;

namespace Audire_MVC.Controllers
{
    public class LocationController : Controller
    {
        //
        // GET: /Region/


        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();

        public ActionResult Location()
        {

            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                if (!string.IsNullOrEmpty(Convert.ToString(Session["locationmessage"])))
                {
                    TempData["locationmessage"] = Convert.ToString(Session["locationmessage"]);
                    Session["locationmessage"] = "";
                }
                else
                {
                    TempData["locationmessage"] = null;
                }
                List<LocationPOCO> listLocation = new List<LocationPOCO>();
                listLocation = objModel.getLocationList();
                return View(listLocation);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        public PartialViewResult EditLocation(int location_id)
        {
            //objCom.LoadEmployee();
            TempData["locationmessage"] = null;
            List<LocationPOCO> listLocation = objModel.getLocationList();

            if (location_id == 0)
            {
                var location = (from s in listLocation.AsEnumerable()
                                select s).ToList();
                return PartialView("EditLocation", location);
            }
            else
            {
                var location = (from s in listLocation.AsEnumerable()
                                where s.location_id == location_id
                                select s).ToList();
                return PartialView("EditLocation", location);
            }
        }

        public PartialViewResult DisplayLocation(string Id, string viewName)
        {
            TempData["locationmessage"] = null;
            List<LocationPOCO> listLocation = new List<LocationPOCO>();
            listLocation = objModel.getLocationList();
            if (string.IsNullOrEmpty(Id) || Id == "0")
            {
                var location = (from s in listLocation.AsEnumerable()
                                select s
                            ).ToList();
                return PartialView("DisplayLocation", location[0]);
            }
            else
            {
                var location = (from s in listLocation.AsEnumerable()
                                where s.location_id == Convert.ToInt32(Id.Trim())
                                select s).ToList();
                return PartialView(viewName, location);
            }

        }

        public PartialViewResult AddNewLocation()
        {
            TempData["locationmessage"] = null;
            LocationPOCO ObjLoc = new LocationPOCO();
            List<LocationPOCO> locationList = new List<LocationPOCO>();
            locationList.Add(ObjLoc);
            return PartialView("EditLocation", locationList);
        }

        public JsonResult GetRegions()
        {
            var dbRegions = datacontext.tbl_region_master.ToList();
            var regions = (from m in dbRegions
                           select new
                           {
                               m.region_id,
                               m.region_name
                           });
            return Json(regions, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCountries(int region_id)
        {
            var dbCountries = datacontext.tbl_country_master.ToList();
            var countries = (from m in dbCountries
                             where m.region_id == (region_id == 0 ? m.region_id : region_id)
                             select new
                             {
                                 m.country_id,
                                 m.country_name
                             }
                           );
            return Json(countries, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocations(int country_id)
        {
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.country_id == (country_id == 0 ? m.country_id : country_id)
                             select new
                             {
                                 m.location_id,
                                 m.location_name
                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLocation(string region, string country, string location_id, string location, string shifts, string st1, string et1, string st2, string et2, string st3, string et3, string timezone)
        {
            ErrorOutput obj = new ErrorOutput();
            int region_id = objModel.getRegionId(region: region);
            int country_id = objModel.getCountryId(country: country);
            if (region_id == 0)
            {
                tbl_region_master objregion = new tbl_region_master();
                objregion.region_name = region;
                objregion.region_code = null;
                datacontext.tbl_region_master.Add(objregion);
                datacontext.SaveChanges();
                region_id = objModel.getRegionId(region: region);
            }

            if (country_id == 0 && region_id > 0)
            {
                tbl_country_master objcountry = new tbl_country_master();
                objcountry.country_name = country;
                objcountry.country_code = country;
                objcountry.region_id = region_id;
                datacontext.tbl_country_master.Add(objcountry);
                datacontext.SaveChanges();
                country_id = objModel.getCountryId(country: country);
            }
            int loc_id=0;
            if (Convert.ToInt32(location_id) == 0)
                loc_id = objModel.getLocationId(location, country_id);
            else loc_id = Convert.ToInt32(location_id);
            if (Convert.ToInt32(loc_id) == 0)
            {
                if (country_id > 0 && region_id > 0)
                {
                    tbl_location_master objLoc = new tbl_location_master();
                    objLoc.country_id = country_id;
                    objLoc.location_name = location;
                    objLoc.region_id = region_id;
                    objLoc.no_of_shifts = Convert.ToInt32(shifts);
                    objLoc.shift1_start_time = TimeSpan.Parse(st1);
                    objLoc.shift1_end_time = TimeSpan.Parse(et1);
                    objLoc.shift2_start_time = TimeSpan.Parse(st2);
                    objLoc.shift2_end_time = TimeSpan.Parse(et2);
                    objLoc.shift3_start_time = TimeSpan.Parse(st3);
                    objLoc.shift3_end_time = TimeSpan.Parse(et3);
                    objLoc.timezone_code = timezone;
                    datacontext.tbl_location_master.Add(objLoc);
                    datacontext.SaveChanges();
                    //obj.err_code = 200;
                    //obj.err_message = "Location details are saved successfully.";
                    TempData["locationmessage"] = "Location details are saved successfully.";
                }
                else
                {
                    //obj.err_code = 202;
                    //obj.err_message = "There is some issue in saving country and region, location details are not saved.";
                    TempData["locationmessage"] = "There is some issue in saving country and region, location details are not saved.";
                }
            }
            else
            {

                tbl_location_master tblloc = new tbl_location_master();
                //int loc_id = Convert.ToInt32(location_id);
                tblloc = datacontext.tbl_location_master.AsQueryable().
                    FirstOrDefault(k => k.location_id == loc_id);
                if (tblloc != null)
                {
                    tblloc.country_id = country_id;
                    tblloc.location_name = location;
                    tblloc.region_id = region_id;
                    tblloc.no_of_shifts = Convert.ToInt32(shifts);
                    tblloc.shift1_start_time = TimeSpan.Parse(st1);
                    tblloc.shift1_end_time = TimeSpan.Parse(et1);
                    tblloc.shift2_start_time = TimeSpan.Parse(st2);
                    tblloc.shift2_end_time = TimeSpan.Parse(et2);
                    tblloc.shift3_start_time = TimeSpan.Parse(st3);
                    tblloc.shift3_end_time = TimeSpan.Parse(et3);
                    tblloc.timezone_code = timezone;
                    datacontext.SaveChanges();
                    //obj.err_code = 200;
                    //obj.err_message = "Location details are updated successfully.";
                    TempData["locationmessage"] = "Location details are updated successfully.";
                }
                else
                {
                    //obj.err_code = 202;
                    //obj.err_message = "No Location is found for update. Please try again.";
                    TempData["locationmessage"] = "No Location is found for update. Please try again.";
                }
            }
            Session["locationmessage"] = Convert.ToString(TempData["locationmessage"]);
            List<LocationPOCO> listLocation = new List<LocationPOCO>();
            listLocation = objModel.getLocationList();
            return View("Location",listLocation);
            //return RedirectToAction("Location", listLocation);
           // return Json(obj, JsonRequestBehavior.AllowGet);
           //return RedirectToAction("Location", "Location");
        }

    }
}

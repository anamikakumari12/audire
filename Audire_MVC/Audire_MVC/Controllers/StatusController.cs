﻿using Audire_MVC.Models;
using Audire_POCO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Audire_MVC.Controllers
{
    public class StatusController : Controller
    {
        //
        // GET: /Status/
        MastersModel objModel = new MastersModel();

        public ActionResult NCStatus()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {
                Session["ddlLocation"] = objModel.getLocationList();
                Session["ddlRegion"] = objModel.getRegionList();
                Session["ddlCountry"] = objModel.getCountryList();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public PartialViewResult DisplayNCStatus(int location_id, int module)
        {
            int user_id = Convert.ToInt32(Session["LoginID"]);
            List<AuditStatus> obj = new List<AuditStatus>();
            obj = objModel.getConsolidatedAuditStatus(location_id, module, user_id);
            return PartialView("DisplayNCStatus", obj);
        }

        [HttpGet]
        public ActionResult DownloadFile(int audit_id)
        {
            string filename = "Audit_Result_" + Convert.ToString(audit_id) + ".pdf";
            string filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
            string completepath = filepath + filename;
            if (System.IO.File.Exists(completepath))
            {
                return File(completepath, "text/plain", filename);
            }
            else
            {
                TempData["statusmessage"] = "File Not Found.";
                return RedirectToAction("NCStatus", "Status");
            }
        }
    }
}

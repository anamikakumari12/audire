﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_EF;
using Audire_POCO;
using Audire_MVC.Models;
using System.ComponentModel.DataAnnotations;
using System.Reflection;
using System.IO;
using Audire_MVC.Utilities;
using System.Data.Objects;
using System.Globalization;


namespace Audire_MVC.Controllers
{
    public class CalendarController : Controller
    {
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();

        // start of old Calendar
        //public ActionResult Calendar()
        //{
        //    if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
        //    {
        //        Session["ddlLocation"] = objModel.getLocationList();
        //        Session["ddlRegion"] = objModel.getRegionList();
        //        Session["ddlCountry"] = objModel.getCountryList();
        //        return View();
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "User");
        //    }
            
        //}

        public PartialViewResult DisplayCalendar()
        {
            return PartialView("DisplayCalendar");
        }
        [HttpPost]
        public JsonResult GetEvents(int location, int module_id)
        {

            List<CalendarEvent> tasksList = new List<CalendarEvent>();
            try
            {

            foreach (sp_GetAuditPlan_Result cevent in datacontext.sp_GetAuditPlan(location,module_id, null, null))
            {

                tasksList.Add(new CalendarEvent
                {
                    audit_plan_id = cevent.audit_plan_id,
                    planned_date = Convert.ToDateTime(cevent.planned_date).ToString("MM-dd-yyyy"),
                    Emp_name = cevent.Emp_name,
                    line_name = cevent.line_name,
                    allDay = false,
                    title = Convert.ToString(cevent.Emp_name) + "(" + Convert.ToString(cevent.Audit_Type) + ")",
                    //+ "(" + Convert.ToString(cevent.module) + ")(Audit_score: " + Convert.ToString(cevent.Audit_Score) + ")",
                    planned_end_date = Convert.ToDateTime(cevent.planned_date_end).ToString("MM-dd-yyyy"),
                    Audit_Status = Convert.ToInt32(cevent.Audit_status),
                    color_code=Convert.ToString(cevent.color_code),
                    module=Convert.ToString(cevent.module),
                    module_id=Convert.ToInt32(cevent.module_id),
                    to_be_audited_by_user_id=Convert.ToInt32(cevent.to_be_audited_by_user_id),
                    Audit_type_id=Convert.ToString(cevent.Audit_type_id)
                }
                );
            }

            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return Json(tasksList.ToArray(), JsonRequestBehavior.AllowGet);

            //DateTime start;
            //DateTime end;
            //var viewModel = new CalendarEvent();
            //var events = new List<CalendarEvent>();
            //start = DateTime.Today.AddDays(-14);
            //end = DateTime.Today.AddDays(-11);

            //for (var i = 1; i <= 5; i++)
            //{
            //    events.Add(new CalendarEvent()
            //    {
            //        audit_plan_id = i,
            //        title = "Event " + i,
            //        planned_date = start.ToString(),
            //        planned_end_date = end.ToString(),
            //        allDay = false
            //    });

            //    start = start.AddDays(7);
            //    end = end.AddDays(7);
            //}
            //return Json(events.ToArray(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePlan(PlanDetails obj)
        {
            try
            {
                DateTime? startdate=null;
                DateTime? enddate=null;
                string UIpattern = "dd-MM-yyyy";
                string DBPattern = "MM-dd-yyyy";
                DateTime parsedDate;
                string reportdate;
                if (DateTime.TryParseExact(obj.planned_date, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    reportdate = parsedDate.ToString(DBPattern);
                    startdate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }
                if (DateTime.TryParseExact(obj.planned_end_date, UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    reportdate = parsedDate.ToString(DBPattern);
                    enddate = DateTime.ParseExact(reportdate, "MM-dd-yyyy", null);
                }

                ObjectParameter l_invite_to_list = new ObjectParameter("l_invite_to_list", typeof(string));
                ObjectParameter l_err_code = new ObjectParameter("l_err_code", typeof(int));
                ObjectParameter l_err_message = new ObjectParameter("l_err_message", typeof(string));
                datacontext.sp_InsAuditPlan(obj.audit_plan_id, obj.user_id, startdate, enddate, obj.location, obj.module_id, obj.audit_type_id, obj.audit_note, l_invite_to_list, l_err_code, l_err_message);
                obj.invite_to_list = Convert.ToString(l_invite_to_list.Value);
                obj.err_code = Convert.ToInt32(l_err_code.Value);
                obj.err_msg = Convert.ToString(l_err_message.Value);
            }
            catch (Exception ex)
            {
                obj.err_code = 202;
                obj.err_msg = Convert.ToString(ex.Message);
                CommonFunctions.ErrorLog(ex);
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        // end of old Calendar

            //start of new Calendar
        public ActionResult PlanCalendar()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {
                Session["ddlLocation"] = objModel.getLocationList();
                Session["ddlRegion"] = objModel.getRegionList();
                Session["ddlCountry"] = objModel.getCountryList();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }

        }

        public PartialViewResult PlanConsolidated(int module_id, int location_id)
        {
            List<PlanRatePOCO> model = new List<PlanRatePOCO>();
            model = objModel.getPlanConsolidated(module_id, location_id);
            return PartialView("PlanConsolidated", model);
        }


        public PartialViewResult PlanDetails(int module_id, int location_id)
        {
            List<CalendarEvent> model = new List<CalendarEvent>();
            model = objModel.getAuditPlans(module_id, location_id);
            return PartialView("PlanDetails", model);
        }


        public JsonResult GetEmployeess(int location_id)
        {
            var dbLocations = datacontext.tbl_user_master.ToList();
            var users = (from m in dbLocations
                             where m.location_id == location_id
                             select new
                             {
                                 m.user_id,
                                 m.emp_full_name
                             });
            return Json(users, JsonRequestBehavior.AllowGet);
        }
    }
}

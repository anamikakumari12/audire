﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_EF;
using Audire_POCO;
using Audire_MVC.Models;

namespace Audire_MVC.Controllers
{
    public class RoleController : Controller
    {
        //
        // GET: /Role/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        public ActionResult Role()
        {

            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                TempData["rolemessage"] = "";
                Session["Permissionlist"] = objModel.getPermissions();
                List<RolePOCO> listRole = objModel.getRoleList();
                return View(listRole);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }
        [HttpGet]
        public JsonResult IsRoleExists(string RoleName, string RoleId)
        {
            int id = Convert.ToInt32(RoleId);
            ErrorOutput objerr = new ErrorOutput();
            bool validateName = (datacontext.tbl_role_master.Any(x => x.Role_name == RoleName));
            if (validateName == true)
            {
                if (id > 0)
                {
                    List<RolePOCO> listRole = new List<RolePOCO>();

                    //listRole = datacontext.tbl_role_master.Where(m => m.role_id == id).
                    //        AsEnumerable().OrderBy(m => m.rolename)
                    //        .Select(c => new RolePOCO
                    //        {
                    //            role_id = c.role_id,
                    //            role_desc = c.rolename
                    //        }).ToList();
                    //if (!(listRole[0].role_desc == RoleName))
                    //{
                    //    objerr.err_message = "Rolename already exist.";
                    //    objerr.err_code = 200;
                    //}

                }
                else
                {
                    objerr.err_message = "Rolename already exist.";
                    objerr.err_code = 200;
                }
            }
            else
            {
            }
            return Json(objerr, JsonRequestBehavior.AllowGet);

        }

        public PartialViewResult EditRole(int? role_id)
        {
            List<RolePOCO> listRole = objModel.getRoleList();

            if (role_id == 0)
            {
                var role = (from s in listRole.AsEnumerable()
                            select s).ToList();
                return PartialView("EditRole", role);
            }
            else
            {
                var role = (from s in listRole.AsEnumerable()
                            where s.role_id == role_id
                            select s).ToList();
                return PartialView("EditRole", role);
            }
        }

        public PartialViewResult DisplayRole(string Id, string viewName)
        {
            List<RolePOCO> listRole = new List<RolePOCO>();
            listRole = objModel.getRoleList();
            if (string.IsNullOrEmpty(Id) || Id == "0")
            {
                var role = (from s in listRole.AsEnumerable()
                            select s
                            ).ToList();
                return PartialView("DisplayRole", role[0]);
            }
            else
            {
                var role = (from s in listRole.AsEnumerable()
                            where s.role_id == Convert.ToInt32(Id.Trim())
                            select s).ToList();
                return PartialView(viewName, role);
            }

        }

        public PartialViewResult AddNewRole()
        {
            RolePOCO ObjEmp = new RolePOCO();
            List<RolePOCO> roleList = new List<RolePOCO>();
            roleList.Add(ObjEmp);
            return PartialView("EditRole", roleList);
        }

        //public JsonResult GetFrequencyValue(int role_id)
        //{
        //    var dbAudit = datacontext.tbl_role_master.ToList();
        //    var audit = (from m in dbAudit
        //                 where m.frequency == Convert.ToString((role_id == 0 ? m.role_id : role_id))
        //                 select new
        //                 {
        //                     m.frequency


        //                 });
        //    return Json(audit, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult SaveRole(RolePOCO objRolePOCO)
        {
            ErrorOutput obj = new ErrorOutput();
            string role_access = "";
            foreach(Permissions m in objRolePOCO.permission)
            {
                role_access = role_access+Convert.ToString(m.ID) + ",";
            }
            role_access = role_access.Substring(0, role_access.Length-1);
            if (objRolePOCO.role_id > 0)
            {
                try
                {
                    tbl_role_master tblrole = new tbl_role_master();
                    tblrole = datacontext.tbl_role_master.AsQueryable().
                        FirstOrDefault(k => k.Role_id == objRolePOCO.role_id);

                    if (tblrole != null)
                    {
                        tblrole.Role_name = objRolePOCO.role_name;
                        tblrole.Role_Access = role_access;
                        datacontext.SaveChanges();

                        List<tbl_user_master> tbluserList = new List<tbl_user_master>();
                        tbluserList = datacontext.tbl_user_master.Where(k => k.role_id == objRolePOCO.role_id).ToList();
                        if (tbluserList.Count > 0)
                        {
                            foreach (tbl_user_master tbluser in tbluserList)
                            {
                                tbluser.role = objRolePOCO.role_name;
                                datacontext.SaveChanges();
                            }
                        }
                        obj.err_code = 200;
                        obj.err_message = "Role details are updated successfully.";
                        // TempData["rolemessage"] = "Role details are updated successfully.";
                    }
                    else
                    {
                        obj.err_code = 201;
                        obj.err_message = "No Role is found for update. Please try again.";
                        //TempData["rolemessage"] = "No Role is found for update. Please try again.";
                    }
                }
                catch (Exception ex)
                {
                    obj.err_code = 201;
                    obj.err_message = "Error in updating the role details. Please try again.";
                    //TempData["rolemessage"] = "Error in updating the role details. Please try again.";
                    //ex.ToString();
                }
            }
            else
            {
                tbl_role_master objRole = new tbl_role_master();
                objRole.Role_name = objRolePOCO.role_name;
                objRole.Role_Access = role_access;
                datacontext.tbl_role_master.Add(objRole);
                datacontext.SaveChanges();
                obj.err_code = 200;
                obj.err_message = "Role details are saved successfully.";
                //TempData["rolemessage"] = "Role details are saved successfully.";
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
           
        }


        [HttpPost]
        public JsonResult SetTempData(string rolemessage)
        {
            // Set your TempData key to the value passed in
            TempData["rolemessage"] = rolemessage;
            return Json(true);
        }

    }
}

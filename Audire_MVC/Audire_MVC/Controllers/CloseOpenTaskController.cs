﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_EF;
using Audire_POCO;
using Audire_MVC.Models;
using System.Web.Services;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Audire_MVC.Utilities;
using Newtonsoft.Json;
using System.Configuration;
using System.Data;
using System.Data.Objects;
using System.Reflection;

namespace Audire_MVC.Controllers
{
    public class CloseOpenTaskController : Controller
    {
        //
        // GET: /CloseOpenTask/
        Audire_dbEntities datacontext = new Audire_dbEntities();

        MastersModel objModel = new MastersModel();

        public ActionResult CloseOpenTask(string audit_id)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && ((Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))) || (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Create))) || (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Update)))))
            {
                Session["ddlLocation"] = objModel.getLocationList();
                Session["ddlRegion"] = objModel.getRegionList();
                Session["ddlCountry"] = objModel.getCountryList();
                Session["ddlLine"] = objModel.getLineList();

                if (!string.IsNullOrEmpty(audit_id))
                {
                    List<ddlOpenTaskPOCO> obj = new List<ddlOpenTaskPOCO>();
                    obj = objModel.GetOpenCloseTaskddl(Convert.ToInt32(audit_id));
                    Session["ddlOpenTask"] = obj;
                }
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public PartialViewResult DisplayOpenCloseTask()
        {

            return PartialView("DisplayPerformAudit");
        }

        public JsonResult GetDropdownselection()
        {
            List<ddlOpenTaskPOCO> obj = new List<ddlOpenTaskPOCO>();
            if (Session["ddlOpenTask"] != null && Session["ddlOpenTask"] != "")
                obj = (List<ddlOpenTaskPOCO>)Session["ddlOpenTask"];
            Session["ddlOpenTask"] = "";
            if (obj.Count > 0)
            {
                return Json(obj[0], JsonRequestBehavior.AllowGet);
            }
            else
            {
                ddlLocationPOCO objLoc = new ddlLocationPOCO();
                if (Session["objLoginLoc"] != null && Session["objLoginLoc"] != "")
                    objLoc = (ddlLocationPOCO)Session["objLoginLoc"];
                return Json(objLoc, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetCountries(int region_id)
        {
            var dbCountries = datacontext.tbl_country_master.ToList();
            var countries = (from m in dbCountries
                             where m.region_id == (region_id == 0 ? m.region_id : region_id)
                             select new
                             {
                                 m.country_id,
                                 m.country_name
                             }
                           );
            return Json(countries, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLocations(int country_id)
        {
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.country_id == (country_id == 0 ? m.country_id : country_id)
                             select new
                             {
                                 m.location_id,
                                 m.location_name
                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetLines(int region_id)
        {
            var dbLines = datacontext.tbl_line_master.ToList();
            var lines = (from m in dbLines
                         where m.region_id == (region_id == 0 ? m.region_id : region_id)
                         select new
                         {
                             m.line_id,
                             m.line_name
                         });
            return Json(lines, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModules_id(string module)
        {
            var dbModule = datacontext.tbl_Module_master.ToList();
            var modules = (from m in dbModule
                           where m.module == (module == "" ? m.module : module)
                           select new
                           {
                               m.module_id
                           });

            return Json(modules, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAuditType(int module_id)
        {
            var dbAudit = datacontext.tbl_audit_type.ToList();
            var audit = (from m in dbAudit
                         where m.module_id == (module_id == 0 ? m.module_id : module_id)
                         orderby m.display_seq
                         select new
                         {
                             m.ID,
                             m.Audit_Type
                         });
            return Json(audit, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        public PartialViewResult GetQuestion(int region_id, int country_id, int location_id, int module_id, int ID)
        {
            List<OpenCloseTaskPOCO> listTask = new List<OpenCloseTaskPOCO>();
            try
            {
                listTask = objModel.GetOpenCloseTask(region_id, country_id, location_id, 0, module_id, ID, Convert.ToInt32(Session["LoginID"]));
                for (int i = 1; i <= listTask.Count; i++)
                {
                    listTask[i - 1].answer_counter = Convert.ToString(i);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return PartialView("DisplayOpenCloseTask", listTask);
        }

        public PartialViewResult Clear()
        {
            ModelState.Clear();
            return PartialView("DisplayOpenCloseTask");
        }

        [HttpPost]
        public ActionResult SaveAuditReview(AuditResult auditResult)
        {
            List<ErrorOutput> myojb = new List<ErrorOutput>();
            try
            {
                string filename = string.Empty;
                string audit_id = string.Empty;
                auditResult = AttachFiles(auditResult);
                audit_id = auditResult.audit_id;
                string json = JsonConvert.SerializeObject(auditResult);
                CommonFunctions.TestLog("json length : " + json.Length);
                string pathString = ConfigurationManager.AppSettings["JsonFolderURL"].ToString();
                filename = audit_id + "_R.json";
                CommonFunctions.TestLog("pathString : " + pathString);
                CommonFunctions.TestLog("filename : " + filename);
                if (!Directory.Exists(pathString))
                {
                    Directory.CreateDirectory(pathString);
                }

                if (System.IO.File.Exists(pathString + filename))
                {
                    System.IO.File.Delete(pathString + filename);
                    System.IO.File.WriteAllText(pathString + filename, json);
                }
                else
                {
                    System.IO.File.WriteAllText(pathString + filename, json);
                }

                CommonFunctions.TestLog("filename : " + filename);
                myojb = callSaveReviewAPI(filename);
                CommonFunctions.TestLog("Count : " + myojb.Count);

                ////string ReportGenerateServiceURL = Convert.ToString("http://localhost:62603/AudireServices.svc/SaveAuditReview");
                //string ReportGenerateServiceURL = Convert.ToString("http://knswin.cloudapp.net/Audire_Service/AudireServices.svc/SaveAuditReview");
                //HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                //request.Method = "POST";
                //request.ContentType = "application/json";
                //using (var sw = new StreamWriter(request.GetRequestStream()))
                //{
                //    JavaScriptSerializer jser = new JavaScriptSerializer();
                //    string json = jser.Serialize(auditResult);
                //    // string json = "{\"audit_id\":\"2\",    \"username\":\"anamika12\",       \"password\":\"ed7b25e54bfc9a4f8e51d9c4c004cfd8\",       \"userID\":\"6\",   \"deviceID\":\"postman\",   \"module_id\":\"1\",   \"region_id\":\"2\",   \"country_id\":\"1\",   \"location_id\":\"1\",   \"line_id\":\"4\",   \"product_id\":\"1\",       \"questionAnswer\":[       {           \"question_id\":\"1\",           \"Answer\":\"0\",           \"comment\":\"test1\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"2\",           \"Answer\":\"0\",           \"comment\":\"test2\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"3\",           \"Answer\":\"0\",           \"comment\":\"test3\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"4\",           \"Answer\":\"1\",           \"comment\":\"test5\",           \"assignedTo\":\"1\"       },       {           \"question_id\":\"5\",           \"Answer\":\"1\",           \"comment\":\"test6\",           \"assignedTo\":\"2\"       },       {           \"question_id\":\"6\",           \"Answer\":\"1\",           \"comment\":\"test7\",           \"assignedTo\":\"3\"       },       {           \"question_id\":\"7\",           \"Answer\":\"0\",           \"comment\":\"test8\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"8\",           \"Answer\":\"0\",           \"comment\":\"test9\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"9\",           \"Answer\":\"0\",           \"comment\":\"test4\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"10\",           \"Answer\":\"0\",           \"comment\":\"test10\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"11\",\"Answer\":\"0\",           \"comment\":\"test11\",          \"assignedTo\":\"\"      },       {           \"question_id\":\"12\",           \"Answer\":\"0\",           \"comment\":\"test12\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"13\",           \"Answer\":\"1\",           \"comment\":\"test13\",           \"assignedTo\":\"4\"       },       {           \"question_id\":\"14\",           \"Answer\":\"0\",           \"comment\":\"test14\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"15\",           \"Answer\":\"0\",          \"comment\":\"test15\",           \"assignedTo\":\"\"      }             ]  }";                    
                //    sw.Write(json);
                //    sw.Flush();
                //}
                //HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                //List<AuditResultOutput> myojb;
                //using (var reader = new StreamReader(response.GetResponseStream()))
                //{
                //    JavaScriptSerializer js = new JavaScriptSerializer();
                //    var objText = reader.ReadToEnd();
                //    myojb = (List<AuditResultOutput>)js.Deserialize(objText, typeof(List<AuditResultOutput>));
                //}

            }
            catch (Exception ex)
            {
                ErrorOutput obj = new ErrorOutput();
                obj.err_code = 202;
                obj.err_message = ex.Message;
                myojb.Add(obj);
                CommonFunctions.ErrorLog(ex);
            }
            return Json(myojb, JsonRequestBehavior.AllowGet);
        }

        public List<ErrorOutput> callSaveReviewAPI(string filename)
        {
            List<ErrorOutput> myojb = new List<ErrorOutput>();
            try
            {
                //string ReportGenerateServiceURL = Convert.ToString("http://localhost:62603/AudireServices.svc/SaveAuditReviewWeb");
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["SaveAuditReview"]);
                //string ReportGenerateServiceURL = Convert.ToString("http://knswin.cloudapp.net/Audire_Service/AudireServices.svc/SaveAuditReviewWeb");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";

                request.ContentType = "application/json";
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    JavaScriptSerializer jser = new JavaScriptSerializer();
                    jser.MaxJsonLength = Int32.MaxValue;
                    string json = "{\"filename\":\"" + filename + "\"}";
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    myojb = (List<ErrorOutput>)js.Deserialize(objText, typeof(List<ErrorOutput>));
                }

            }
            catch (Exception ex)
            {
                ErrorOutput obj = new ErrorOutput();
                obj.err_code = 202;
                obj.err_message = ex.Message;
                myojb.Add(obj);
                CommonFunctions.ErrorLog(ex);
            }
            return myojb;
        }
        private AuditResult AttachFiles(AuditResult auditResult)
        {
            try
            {
                List<ReviewImage> objList = (List<ReviewImage>)Session["ReviewImage"];
                List<reviewStatus> objreviewStatus = auditResult.reviewStatus;
                if (objList != null)
                {
                    if (objList.Count > 0)
                    {
                        if (objreviewStatus.Count > 0)
                            for (int i = 0; i < objreviewStatus.Count; i++)
                            {
                                for (int j = 0; j < objList.Count; j++)
                                {
                                    if (objList[j].answer_counter == i + 1)
                                        objreviewStatus[i].review_image_file_name = Convert.ToString(objList[j].image);
                                }
                            }
                    }
                }
                auditResult.reviewStatus = objreviewStatus;
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return auditResult;
        }

        [HttpPost]
        //HttpPostedFileBase file, int question_id
        public JsonResult UploadFile()
        {
            try
            {
                HttpFileCollectionBase files = Request.Files;
                HttpPostedFileBase file = files[0];
                if (file.ContentLength > 0)
                {
                    List<ReviewImage> objList = (List<ReviewImage>)Session["ReviewImage"];

                    System.IO.Stream fs = file.InputStream;
                    System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    CommonFunctions.TestLog(Convert.ToString(base64String.Length));
                    if (objList == null)
                    {
                        objList = new List<ReviewImage>();
                    }
                    CommonFunctions.TestLog(Convert.ToString(objList.Count));
                    ReviewImage obj = new ReviewImage();
                    obj.answer_counter = Convert.ToInt32(files.AllKeys[0]);
                    obj.image = base64String;
                    objList.Add(obj);
                    Session["ReviewImage"] = objList;
                    CommonFunctions.TestLog(Convert.ToString(objList.Count));
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return Json(true);
        }

        [HttpPost]
        public JsonResult SetTempData(string OpenClosemessage)
        {
            // Set your TempData key to the value passed in
            TempData["OpenClosemessage"] = OpenClosemessage;
            return Json(true);
        }


        [HttpPost]
        //HttpPostedFileBase file, int question_id
        public JsonResult UploadExceFile()
        {
            List<ErrorOutput> myojb = new List<ErrorOutput>();
            try
            {
                HttpFileCollectionBase files = Request.Files;
                HttpPostedFileBase file = files[0];
                CommonFunctions.TestLog("UploadExcelFile");
                if (file.ContentLength > 0)
                {
                    CommonFunctions.TestLog("UploadExcelFile1");
                    DataTable dt = ReadCsvFile(file);
                    dt.Columns.Add("review_closed_status");
                    dt.Columns.Add("review_comments");
                    dt.Columns.Add("review_image_file_name");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (!string.IsNullOrEmpty(Convert.ToString(dt.Rows[i]["Review_status"])))
                        {
                            dt.Rows[i]["review_closed_status"] = Convert.ToString(dt.Rows[i]["Review_status"]).Trim().ToUpper() == "OPEN" ? "0" : "1";
                            dt.Rows[i]["review_comments"] = Convert.ToString(dt.Rows[i]["Review_Comment"]);
                        }
                    }
                    dt.Columns.Remove("module");
                    dt.Columns.Remove("Audit_Type");
                    dt.Columns.Remove("location_name");
                    dt.Columns.Remove("Auditor");
                    dt.Columns.Remove("question");
                    dt.Columns.Remove("Auditor_comment");
                    dt.Columns.Remove("Process");
                    dt.Columns.Remove("Review_status");
                    dt.Columns.Remove("Review_Comment");
                    //dt.Columns.Remove("IsCompleted");
                    List<BulkReviewList> items = DataTableToList<BulkReviewList>(dt);
                    //                    dt.AsEnumerable().Select(row =>
                    //new BulkReviewList
                    //{
                    //    audit_id = row.Field<int>("audit_id"),
                    //    question_id = row.Field<int>("question_id"),
                    //    review_closed_on = row.Field<string>("review_closed_on"),
                    //    review_closed_status = row.Field<int>("review_closed_status"),
                    //    review_comments = row.Field<string>("review_comments"),
                    //    review_image_file_name = row.Field<string>("review_image_file_name")
                    //}).ToList();

                    BulkReview obj = new BulkReview();
                    obj.deviceID = "Web";
                    obj.userID = Convert.ToString(Session["LoginID"]);
                    obj.reviewObj = items;

                    myojb = callBulkUploadReviewAPI(obj);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return Json(myojb, JsonRequestBehavior.AllowGet);
        }


        public List<ErrorOutput> callBulkUploadReviewAPI(BulkReview obj)
        {
            List<ErrorOutput> myojb = new List<ErrorOutput>();
            try
            {
                //string ReportGenerateServiceURL = Convert.ToString("http://localhost:62603/AudireServices.svc/UploadBulkReview");
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["UploadBulkReview"]);
                    //Convert.ToString("http://knswin.cloudapp.net/Audire_Service/AudireServices.svc/UploadBulkReview");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";

                request.ContentType = "application/json";
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    JavaScriptSerializer jser = new JavaScriptSerializer();
                    jser.MaxJsonLength = Int32.MaxValue;
                    string json = jser.Serialize(obj);
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    myojb = (List<ErrorOutput>)js.Deserialize(objText, typeof(List<ErrorOutput>));
                }

            }
            catch (Exception ex)
            {
                ErrorOutput err_obj = new ErrorOutput();
                err_obj.err_code = 202;
                err_obj.err_message = ex.Message;
                myojb.Add(err_obj);
                CommonFunctions.ErrorLog(ex);
            }
            return myojb;
        }

        public DataTable ReadCsvFile(HttpPostedFileBase file)
        {

            DataTable dtCsv = new DataTable();
            string Fulltext;

            string FileSaveWithPath = Server.MapPath("\\Files\\Import" + System.DateTime.Now.ToString("ddMMyyyy_hhmmss") + ".csv");
            file.SaveAs(FileSaveWithPath);
            using (StreamReader sr = new StreamReader(FileSaveWithPath))
            {
                while (!sr.EndOfStream)
                {
                    Fulltext = sr.ReadToEnd().ToString(); //read full file text  
                    string[] rows = Fulltext.Split('\n'); //split full file text into rows  
                    for (int i = 0; i < rows.Count() - 1; i++)
                    {
                        string[] rowValues = rows[i].Split(','); //split each row with comma to get individual values  
                        {
                            if (i == 0)
                            {
                                for (int j = 0; j < rowValues.Count(); j++)
                                {
                                    dtCsv.Columns.Add(rowValues[j].Trim()); //add headers  
                                }
                            }
                            else
                            {
                                DataRow dr = dtCsv.NewRow();
                                for (int k = 0; k < rowValues.Count(); k++)
                                {
                                    dr[k] = Convert.ToString(rowValues[k]).Trim();
                                }
                                dtCsv.Rows.Add(dr); //add other rows  
                            }
                        }
                    }
                }
            }

            return dtCsv;
        }

        public List<T> DataTableToList<T>(DataTable table) where T : class, new()
        {
            try
            {
                List<T> list = new List<T>();

                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();

                    foreach (var prop in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(prop.Name);
                            propertyInfo.SetValue(obj, Convert.ChangeType(row[prop.Name], propertyInfo.PropertyType), null);
                        }
                        catch
                        {
                            continue;
                        }
                    }

                    list.Add(obj);
                }

                return list;
            }
            catch
            {
                return null;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_EF;
using Audire_POCO;
using Audire_MVC.Models;
using System.Web.Services;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using System.Data;
using Newtonsoft.Json;
using System.IO;
using System.Text;
using System.Configuration;
using System.Data.Objects;
using Audire_MVC.Utilities;

namespace Audire_MVC.Controllers
{
    public class PerformAuditController : Controller
    {
        //
        // GET: /PerformAudit/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        DataTable dtImage = new DataTable();

        public ActionResult PerformAudit()
        {

            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {
                Session["ddlLocation"] = objModel.getLocationList();
                Session["ddlRegion"] = objModel.getRegionList();
                Session["ddlCountry"] = objModel.getCountryList();
                Session["ddlLine"] = objModel.getLineList();
                Session["ddlProcess"] = objModel.getProcessList();
                Session["ddlAudit_Type"] = objModel.getAuditType();
                return View();
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public PartialViewResult DisplayPerformAudit()
        {
            //Session["ddlLine"] = objModel.getLineList();
            return PartialView("DisplayPerformAudit");
        }

        public PartialViewResult Score()
        {
            return PartialView("Score");
        }

        public JsonResult GetParts1()
        {
            var dbRegions = datacontext.tbl_product_master.ToList();
            var regions = (from m in dbRegions
                           select new
                           {
                               m.product_id,
                               m.product_name
                           });
            return Json(regions, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetParts(int line_id)
        {
            var dbParts = (from pd in datacontext.tbl_product_master
                           join od in datacontext.tbl_line_product_relationship on pd.product_id equals od.product_id

                           select new
                           {
                               pd.product_id,
                               pd.product_name,
                               od.line_id
                           }).ToList();
            var parts = (from m in dbParts
                         where m.line_id == (line_id == 0 ? m.line_id : line_id)
                         select new
                         {
                             m.product_id,
                             m.product_name
                         });
            return Json(parts, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDropdownselection()
        {
            ddlLocationPOCO objLoc = new ddlLocationPOCO();
            if (Session["objLoginLoc"] != null && Session["objLoginLoc"] != "")
                objLoc = (ddlLocationPOCO)Session["objLoginLoc"];
            return Json(objLoc, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCountries(int region_id)
        {
            var dbCountries = datacontext.tbl_country_master.ToList();
            if (region_id == 0)
            {
                var countries = (from m in dbCountries
                                 //where m.region_id == (region_id == 0 ? m.region_id : region_id)
                                 select new
                                 {
                                     m.country_id,
                                     m.country_name
                                 }
                         );
                return Json(countries, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var countries = (from m in dbCountries
                                 where m.region_id == region_id 
                                 select new
                                 {
                                     m.country_id,
                                     m.country_name
                                 }
                               );
                return Json(countries, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetLocations(int country_id)
        {
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.country_id == (country_id == 0 ? m.country_id : country_id)
                             select new
                             {
                                 m.location_id,
                                 m.location_name
                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetShifts(int location_id)
        {
            var dbLocations = datacontext.tbl_location_master.ToList();
            var locations = (from m in dbLocations
                             where m.location_id == (location_id == 0 ? m.location_id : location_id)
                             select new
                             {
                                 m.no_of_shifts

                             });
            return Json(locations, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModules_id(string module)
        {
            var dbModule = datacontext.tbl_Module_master.ToList();
            var modules = (from m in dbModule
                           where m.module == (module == "" ? m.module : module)
                           select new
                           {
                               m.module_id
                           });

            return Json(modules, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult SetModuleSession(string module)
        {
            Session["module"] = module;

            return Json(true, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetModuleSession()
        {
            ModulePOCO objModule = new ModulePOCO();
            objModule.module_id = Convert.ToInt32(Session["module"]);
            return Json(objModule, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetPartsForProductAudit(int module_id, int audit_type)
        {

            var dbAudit = datacontext.tbl_audit_type.ToList();
            var audit = (from m in dbAudit
                         where m.module_id == (module_id == 0 ? m.module_id : module_id)
                         && m.ID == (audit_type == 0 ? m.ID : audit_type)
                         select new
                         {
                             m.Parts
                         });
            return Json(audit, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetAuditType(int module_id)
        {

            var dbAudit = datacontext.tbl_audit_type.ToList();
            var audit = (from m in dbAudit
                         where m.module_id == (module_id == 0 ? m.module_id : module_id)
                         orderby m.display_seq
                         select new
                         {
                             m.ID,
                             m.Audit_Type

                         });
            return Json(audit, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public PartialViewResult GetQuestion(int region_id, int country_id, int location_id, int module_id, int ID, string part)
        //public PartialViewResult GetQuestion(AuditResult auditResult)
        {
            List<PerformAuditPOCO> listquestion = new List<PerformAuditPOCO>();
            try
            {
                AuditResult auditResult = new AuditResult();
                auditResult.audit_id = "0";
                auditResult.line_id = "0";
                auditResult.product_id = "0";
                auditResult.auditType_id = ID;
                auditResult.country_id = Convert.ToString(country_id);
                auditResult.region_id = Convert.ToString(region_id);
                auditResult.location_id = Convert.ToString(location_id);
                auditResult.module_id = Convert.ToString(module_id);
                auditResult.username = Convert.ToString(CommonDropdowns.GetUsername());
                auditResult.password = Convert.ToString(CommonDropdowns.GetPassword());
                auditResult.userID = Convert.ToString(CommonDropdowns.GetUserid());
                auditResult.deviceID = Convert.ToString("Web");
                auditResult.part = Convert.ToString(part);
                Session["AuditResult"] = auditResult;
                dtImage = new DataTable();
                dtImage.Columns.Add("question_id");
                dtImage.Columns.Add("Image");
                if (module_id == 3)
                {
                    listquestion = (from m in datacontext.tbl_question_master
                                    where m.module_id == module_id && (m.Audit_type_id == ID || m.Audit_type_id == null)
                                    select new PerformAuditPOCO
                                    {
                                        id = m.id,
                                        question_id = m.question_id,
                                        question = m.question,
                                        help_text = m.help_text
                                    }).ToList();
                }
                else
                {
                    var maxValue=0;
                    try
                    {
                        maxValue = datacontext.tbl_AuditResult.Where(o => o.region_id == region_id && o.country_id == country_id && o.location_id == location_id && o.module_id == module_id && o.AuditType_id == ID && o.IsCompleted != "Y")
                            .Max(x => x.audit_id);
                    }
                    catch(Exception ex)
                    {
                        CommonFunctions.ErrorLog(ex);
                    }
                    
                        listquestion = (from m in datacontext.tbl_question_master
                                        join ans in datacontext.tbl_AuditResult
                                        .Where(o => o.audit_id == maxValue)
                                        on m.question_id equals ans.question_id into t
                                        from rt in t.DefaultIfEmpty()
                                        orderby m.question_id
                                        where m.module_id == module_id && m.Audit_type_id == ID
                                        select new PerformAuditPOCO
                                        {
                                            id = m.id,
                                            question_id = m.question_id,
                                            question = m.question,
                                            help_text = m.help_text,
                                            answer = rt.answer,
                                            assignedTo = rt.assigned_To,
                                            comment = rt.audit_comment,
                                            auditor_comment = rt.audit_comment,
                                            audit_id = maxValue,
                                            filename = rt.audit_images
                                            //username = Convert.ToString(Session["LoginUsername"]),
                                            //password = Convert.ToString(Session["Password"])
                                        }).ToList();
                    
                }
                Session["questioncount"] = listquestion.Count;
                for (int i = 1; i <= listquestion.Count; i++)
                {
                    listquestion[i - 1].question_counter = Convert.ToString(i);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return PartialView("DisplayPerformAudit", listquestion);
        }

        [HttpPost]
        public ActionResult GetQuestionJson(int region_id, int country_id, int location_id, int line_id, int module_id, int ID)
        {
            List<PerformAuditPOCO> listquestion = new List<PerformAuditPOCO>();
            listquestion = (from m in datacontext.tbl_question_master
                            where m.module_id == module_id && m.Audit_type_id == ID
                            select new PerformAuditPOCO
                            {
                                id = m.id,
                                question_id = m.question_id,
                                question = m.question,
                                //username = Convert.ToString(Session["LoginUsername"]),
                                //password = Convert.ToString(Session["Password"])
                            }).ToList();

            Session["questioncount"] = listquestion.Count;
            for (int i = 1; i <= listquestion.Count; i++)
            {
                listquestion[i - 1].question_counter = Convert.ToString(i);
            }
            //return PartialView("DisplayPerformAudit", listquestion);
            return Json(listquestion, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        //HttpPostedFileBase file, int question_id
        public JsonResult UploadFile()
        {
            try
            {
                HttpFileCollectionBase files = Request.Files;
                HttpPostedFileBase file = files[0];
                CommonFunctions.TestLog("UploadFile");
                if (file.ContentLength > 0)
                {
                    CommonFunctions.TestLog("UploadFile1");
                    List<QuestionsImage> objList = (List<QuestionsImage>)Session["QuestionsImage"];

                    System.IO.Stream fs = file.InputStream;
                    System.IO.BinaryReader br = new System.IO.BinaryReader(fs);
                    Byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    CommonFunctions.TestLog("UploadFile2");
                    CommonFunctions.TestLog(Convert.ToString(base64String.Length));
                    //Image1.ImageUrl = "data:image/png;base64," + base64String;
                    //Image1.Visible = true;

                    if (objList == null)
                    {
                        objList = new List<QuestionsImage>();
                    }
                    CommonFunctions.TestLog(Convert.ToString(objList.Count));
                    QuestionsImage obj = new QuestionsImage();
                    obj.question_counter = Convert.ToInt32(files.AllKeys[0]);
                    obj.image = base64String;
                    objList.Add(obj);
                    Session["QuestionsImage"] = objList;
                    CommonFunctions.TestLog(Convert.ToString(objList.Count));
                    //string _FileName = Path.GetFileName(file.FileName);  
                    //string _path = Path.Combine(Server.MapPath("~/UploadedFiles"), _FileName);  
                    //file.SaveAs(_path);  


                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return Json(true);
        }

        [HttpPost]
        //FormCollection form
        //public ActionResult SaveQuestionAnswers(List<QuestionAnswer> questionAnswer, string Auditor_comment)
        public ActionResult SaveQuestionAnswers(QuestionAnswerWithComment questionAnswerWC)
        {
            List<AuditResultOutput> myojb = new List<AuditResultOutput>();
            string filename = string.Empty;
            //int NextAudit_id;
            List<QuestionAnswer> questionAnswer = new List<QuestionAnswer>();
            try
            {
                questionAnswer = questionAnswerWC.questionAnswer;
                string audit_id = string.Empty;
                if (!string.IsNullOrEmpty(Convert.ToString(questionAnswerWC.audit_id)))
                {
                    if (questionAnswerWC.audit_id > 0)
                    {
                        audit_id = Convert.ToString(questionAnswerWC.audit_id);
                    }
                    else
                    {
                        ObjectParameter NextAudit_id = new ObjectParameter("p_audit_id", typeof(int));
                        datacontext.GetMaxAuditId(NextAudit_id);
                        audit_id = Convert.ToString(NextAudit_id.Value);
                    }
                }
                else
                {
                    ObjectParameter NextAudit_id = new ObjectParameter("p_audit_id", typeof(int));
                    datacontext.GetMaxAuditId(NextAudit_id);
                    audit_id = Convert.ToString(NextAudit_id.Value);
                }
                CommonFunctions.TestLog("audit_id : " + audit_id);
                AuditResult auditResult = (AuditResult)Session["AuditResult"];
                auditResult.Auditor_comment = Convert.ToString(questionAnswerWC.Auditor_comment);
                auditResult.IsCompleted = Convert.ToString(questionAnswerWC.IsCompleted);
                //List<QuestionAnswer> questionAnswer = new List<QuestionAnswer>();
                //questionAnswer = auditResult.questionAnswer;
                CommonFunctions.TestLog("questionCount : " + questionAnswer.Count);
                List<QuestionsImage> objList = (List<QuestionsImage>)Session["QuestionsImage"];
                //for (int i = 1; i <= Convert.ToInt32(Session["questioncount"]); i++)
                //{
                //    objquest = new QuestionAnswer();
                //    objquest.Answer = Convert.ToString(Request["correct" + i]);
                //    objquest.question_id = Convert.ToString(Request["question_id" + i]);
                //    objquest.comment = Convert.ToString(Request["Comment" + i]);
                //    if (objquest.Answer == "1" || objquest.Answer == "11")
                //        objquest.assignedTo = Convert.ToString(Request["Process" + i]);
                //    else
                //        objquest.assignedTo = "";
                //    if (objList != null)
                //    {
                //        if (objList.Count > 0)
                //        {
                //            for (int j = 0; j < objList.Count; j++)
                //            {
                //                if(objList[j].question_counter==i)
                //                objquest.img_content = Convert.ToString(objList[j].image);
                //            }
                //        }
                //    }
                //    //objquest.img_content = Convert.ToString(Request["base64" + i]);
                //    if (objquest.Answer != null && objquest.Answer != "")
                //        questionAnswer.Add(objquest);

                //}

                if (objList != null)
                {
                    if (objList.Count > 0)
                    {
                        if (questionAnswer.Count > 0)
                            for (int i = 0; i < questionAnswer.Count; i++)
                            {
                                for (int j = 0; j < objList.Count; j++)
                                {
                                    if (objList[j].question_counter == i + 1)
                                        questionAnswer[i].img_content = Convert.ToString(objList[j].image);
                                }
                            }
                    }
                }
                auditResult.questionAnswer = questionAnswer;
                CommonFunctions.TestLog("Audit Type : " + auditResult.auditType_id);
                string json = JsonConvert.SerializeObject(auditResult);
                CommonFunctions.TestLog("json length : " + json.Length);
                //string path = @"G:\Keys\employee.json";
                //string fn = "test.json";
                string pathString = ConfigurationManager.AppSettings["JsonFolderURL"].ToString();
                filename = audit_id + ".json";
                CommonFunctions.TestLog("pathString : " + pathString);
                CommonFunctions.TestLog("filename : " + filename);
                if (!Directory.Exists(pathString))
                {
                    Directory.CreateDirectory(pathString);
                }

                if (System.IO.File.Exists(pathString + filename))
                {
                    System.IO.File.Delete(pathString + filename);
                    System.IO.File.WriteAllText(pathString + filename, json);
                }
                else
                {
                    System.IO.File.WriteAllText(pathString + filename, json);
                }

                CommonFunctions.TestLog("filename : " + filename);
                myojb = callSaveAuditAPI(filename);
                CommonFunctions.TestLog("Count : " + myojb.Count);
            }
            catch (Exception ex)
            {
                AuditResultOutput obj = new AuditResultOutput();
                obj.err_code = 202;
                obj.err_message = ex.Message;
                myojb.Add(obj);
                //throw ex;
                CommonFunctions.ErrorLog(ex);
            }
            return Json(myojb, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAuditResult(AuditResult auditResult)
        {
            Session["AuditResult"] = auditResult;
            return Json(true);
            //return SaveQuestionAnswers();
        }
        private Stream TestStream(string path)
        {
            Stream fs = System.IO.File.OpenRead(path);
            return fs;
        }
        public List<AuditResultOutput> callSaveAuditAPI(string filename)
        {
            List<AuditResultOutput> myojb = new List<AuditResultOutput>();
            try
            {
                //string ReportGenerateServiceURL = Convert.ToString("http://localhost:62603/AudireServices.svc/SaveAuditResult");
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["SaveAuditResult"]);
                    //Convert.ToString("http://knswin.cloudapp.net/Audire_Service/AudireServices.svc/SaveAuditResultWeb");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";

                request.ContentType = "application/json";
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    JavaScriptSerializer jser = new JavaScriptSerializer();
                    jser.MaxJsonLength = Int32.MaxValue;
                    string json = "{\"filename\":\"" + filename + "\"}";
                    //string json = jser.Serialize(auditResult);
                    // string json = "{\"audit_id\":\"2\",    \"username\":\"anamika12\",       \"password\":\"ed7b25e54bfc9a4f8e51d9c4c004cfd8\",       \"userID\":\"6\",   \"deviceID\":\"postman\",   \"module_id\":\"1\",   \"region_id\":\"2\",   \"country_id\":\"1\",   \"location_id\":\"1\",   \"line_id\":\"4\",   \"product_id\":\"1\",      \"shift_no\":\"1\",   \"questionAnswer\":[       {           \"question_id\":\"1\",           \"Answer\":\"0\",           \"comment\":\"test1\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"2\",           \"Answer\":\"0\",           \"comment\":\"test2\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"3\",           \"Answer\":\"0\",           \"comment\":\"test3\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"4\",           \"Answer\":\"1\",           \"comment\":\"test5\",           \"assignedTo\":\"1\"       },       {           \"question_id\":\"5\",           \"Answer\":\"1\",           \"comment\":\"test6\",           \"assignedTo\":\"2\"       },       {           \"question_id\":\"6\",           \"Answer\":\"1\",           \"comment\":\"test7\",           \"assignedTo\":\"3\"       },       {           \"question_id\":\"7\",           \"Answer\":\"0\",           \"comment\":\"test8\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"8\",           \"Answer\":\"0\",           \"comment\":\"test9\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"9\",           \"Answer\":\"0\",           \"comment\":\"test4\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"10\",           \"Answer\":\"0\",           \"comment\":\"test10\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"11\",\"Answer\":\"0\",           \"comment\":\"test11\",          \"assignedTo\":\"\"      },       {           \"question_id\":\"12\",           \"Answer\":\"0\",           \"comment\":\"test12\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"13\",           \"Answer\":\"1\",           \"comment\":\"test13\",           \"assignedTo\":\"4\"       },       {           \"question_id\":\"14\",           \"Answer\":\"0\",           \"comment\":\"test14\",           \"assignedTo\":\"\"       },       {           \"question_id\":\"15\",           \"Answer\":\"0\",          \"comment\":\"test15\",           \"assignedTo\":\"\"      }             ]  }";
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    myojb = (List<AuditResultOutput>)js.Deserialize(objText, typeof(List<AuditResultOutput>));
                }
                //return Json(myojb, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                AuditResultOutput obj = new AuditResultOutput();
                obj.err_code = 202;
                obj.err_message = ex.Message;
                myojb.Add(obj);
                //throw ex;
                CommonFunctions.ErrorLog(ex);
            }
            return myojb;
        }

        public static T GetModelFromJsonRequest<T>(HttpRequestBase request)
        {
            string result = "";
            using (Stream req = request.InputStream)
            {
                req.Seek(0, System.IO.SeekOrigin.Begin);
                result = new StreamReader(req).ReadToEnd();
            }
            return JsonConvert.DeserializeObject<T>(result);
        }

        [HttpPost]
        public JsonResult StoreBase64(HttpPostedFileBase base64, string question_id)
        {
            try
            {
                DataRow dr = dtImage.NewRow();
                dr["question_id"] = question_id;
                dr["Image"] = base64;
                dtImage.Rows.Add(dr);

            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return Json(true);
        }

        public PartialViewResult Clear()
        {
            ModelState.Clear();
            return PartialView("DisplayOpenCloseTask");
        }


        [HttpPost]
        public JsonResult SendReport(string audit_id)
        {
            csSendReport myojb = new csSendReport();
            List<ErrorOutput> objOutput = new List<ErrorOutput>();
            try
            {
                //string ReportGenerateServiceURL = Convert.ToString("http://localhost:62603/AudireServices.svc/SaveAuditResult");
                string ReportGenerateServiceURL = Convert.ToString(ConfigurationManager.AppSettings["SendReport"]);
                //Convert.ToString("http://knswin.cloudapp.net/Audire_Service/AudireServices.svc/SendReport");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(ReportGenerateServiceURL);
                request.Method = "POST";

                request.ContentType = "application/json";
                myojb.audit_id = audit_id;
                myojb.username = Convert.ToString(Session["LoginUsername"]);
                myojb.password = Convert.ToString(Session["Password"]);
                myojb.userID = Convert.ToString(Session["LoginID"]);
                myojb.deviceID = "Web";
                using (var sw = new StreamWriter(request.GetRequestStream()))
                {
                    JavaScriptSerializer jser = new JavaScriptSerializer();
                    jser.MaxJsonLength = Int32.MaxValue;
                    //string json = "{\"filename\":\"" + audit_id + "\"}";
                    string json = jser.Serialize(myojb);
                    sw.Write(json);
                    sw.Flush();
                }
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                using (var reader = new StreamReader(response.GetResponseStream()))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var objText = reader.ReadToEnd();
                    objOutput = (List<ErrorOutput>)js.Deserialize(objText, typeof(List<ErrorOutput>));
                }


            }
            catch (Exception ex)
            {
                ErrorOutput obj = new ErrorOutput();
                obj.err_code = 202;
                obj.err_message = ex.Message;
                objOutput.Add(obj);
                //throw ex;
                CommonFunctions.ErrorLog(ex);
            }
            return Json(objOutput, JsonRequestBehavior.AllowGet);
        }

        public IHtmlString Raw(string value)
        {
            return new HtmlString(value);
        }
    }
}

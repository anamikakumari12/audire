﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_POCO;
using Audire_MVC.Models;

namespace Audire_MVC.Controllers
{
    public class CAPAController : Controller
    {
        //
        // GET: /CAPA/
        MastersModel objModel = new MastersModel();
        public ActionResult CAPAReport()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {
                List<SubAudit> listAudit = new List<SubAudit>();
                ParamPOCO param = new ParamPOCO();
                param.module_id = Convert.ToInt32(Session["module"]);
                listAudit = objModel.getSubAuditType(param);
                return View(listAudit);
            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public PartialViewResult LoadCAPAReport(int auditType, string auditType_name)
        {
            List<CAPA_POCO> list = new List<CAPA_POCO>();
            ParamPOCO param = new ParamPOCO();
            param.module_id = Convert.ToInt32(Session["module"]);
            param.user_id= Convert.ToInt32(Session["LoginID"]);
            param.subAudit_id = auditType;
            param.subAudit_name = auditType_name;
            list = objModel.getCAPAReport(param);
            return PartialView("LoadCAPAReport",list);
        }

        [HttpPost]
        public JsonResult SaveConfirmation(string audit_id, string confirm)
        {
            Confirmation objParam = new Confirmation();
            objParam.audit_id = Convert.ToInt32(audit_id);
           // objParam.question_id = Convert.ToInt32(question_id);
            objParam.confirm_comment = confirm;
            objParam.confirmed_by= Convert.ToInt32(Session["LoginID"]);
            objParam = objModel.SaveConfirmComment(objParam);
            return Json(objParam, JsonRequestBehavior.AllowGet);
        }

    }
}

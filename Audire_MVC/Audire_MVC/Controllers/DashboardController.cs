﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_MVC.Utilities;
using Audire_POCO;
using Audire_MVC.Models;


namespace Audire_MVC.Controllers
{
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        MastersModel objModel = new MastersModel();

        public ActionResult Dashboard(string userId)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
            {

                List<DashboardPOCO> list = new List<DashboardPOCO>();
                List<DashboardPOCO> list1 = new List<DashboardPOCO>();
                List<ModulePOCO> listModule = new List<ModulePOCO>();
                CommonDropdowns objCom = new CommonDropdowns();
                try
                {
                    if (!string.IsNullOrEmpty(userId))
                    {
                        objCom.SetSessionForMobileUser(userId);
                    }
                    string accessedModule = Convert.ToString(Session["ModuleAccessed"]);
                    List<int> ModuleIds = accessedModule.Split(',').Select(int.Parse).ToList();
                    listModule = (List<ModulePOCO>)Session["ddlModule"];
                    //DashboardPOCO obj = new DashboardPOCO();
                    //obj.Module_name = "5S Audit";
                    //obj.Module_id = "2";
                    //obj.detail = GetDashboardDetails(Convert.ToString(obj.Module_id),"Monthly", "Week", Convert.ToString(obj.Module_name));
                    //list.Add(obj);

                    list1 = listModule.AsEnumerable().Where(c => ModuleIds.Contains(c.module_id)).Select(c => new DashboardPOCO
                    {
                        Module_id = Convert.ToString(c.module_id),
                        Module_name = c.module,
                        detail = GetDashboardDetails(Convert.ToString(c.module_id), "Monthly", "Week", Convert.ToString(c.module))
                    }).ToList();
                    foreach (DashboardPOCO ob in list1)
                    {
                        if (ob.Module_id == "4")
                            ob.CssClass = "panel-featured-left panel-featured-primary";
                        else if (ob.Module_id == "1")
                            ob.CssClass = "panel-featured-left panel-featured-second";
                        else if (ob.Module_id == "2")
                            ob.CssClass = "panel-featured-left panel-featured-three";
                        else
                            ob.CssClass = "panel-featured-left panel-featured-four";

                        list.Add(ob);
                    }
                }
                catch (Exception ex)
                {
                    CommonFunctions.ErrorLog(ex);
                }
                return View(list);

            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        private DashboardDetailPOCO GetDashboardDetails(string module_id, string freq, string recSchedule, string module_name)
        {
            DashboardDetailPOCO listDetails = new DashboardDetailPOCO();
            List<AuditTypePOCO> listScore = new List<AuditTypePOCO>();
            List<AuditTasks> listTask = new List<AuditTasks>();
            List<PendingAuditsPOCO> listpendings = new List<PendingAuditsPOCO>();
            listDetails = objModel.getDashboardDetail(module_id, module_name, freq);
            listScore = objModel.getAuditTypeScores(module_id, recSchedule, module_name);
            listTask = objModel.getAllTasks();
            listpendings = objModel.getPendingAudits(Convert.ToInt32(Session["LoginID"]), module_id);
            listDetails.AuditScores = listScore;
            listDetails.Tasks = listTask;
            listDetails.upcomingAudits = listpendings;
            DashboardPOCO obj = new DashboardPOCO();
            obj.detail = listDetails;
            return listDetails;
        }

        public PartialViewResult DashboardGraph(string Module_id, string Module_name)
        {
            Session["DashboardModule_id"] = Module_id;
            List<DashboardPOCO> list = new List<DashboardPOCO>();
            DashboardDetailPOCO listDetails = GetDashboardDetails(Module_id, "Monthly", "Week", Module_name);
            DashboardPOCO obj = new DashboardPOCO();
            obj.detail = listDetails;
            list.Add(obj);
            return PartialView("DashboardGraph", list);
        }

        public PartialViewResult DashboardGraphForMobile(string Module_id, string Module_name)
        {
            Session["DashboardModule_id"] = Module_id;
            List<DashboardPOCO> list = new List<DashboardPOCO>();
            DashboardDetailPOCO listDetails = GetDashboardDetails(Module_id, "Monthly", "Week", Module_name);
            DashboardPOCO obj = new DashboardPOCO();
            obj.detail = listDetails;
            list.Add(obj);
            return PartialView("DashboardGraphForMobile", list);
        }

        public ActionResult MobileDashboard(string userId)
        {
            List<DashboardPOCO> list = new List<DashboardPOCO>();
            List<DashboardPOCO> list1 = new List<DashboardPOCO>();
            List<ModulePOCO> listModule = new List<ModulePOCO>();
            CommonDropdowns objCom = new CommonDropdowns();
            try
            {
                if (!string.IsNullOrEmpty(userId))
                {
                    objCom.SetSessionForMobileUser(userId);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])))
                {
                    string accessedModule = Convert.ToString(Session["ModuleAccessed"]);
                    List<int> ModuleIds = accessedModule.Split(',').Select(int.Parse).ToList();
                    listModule = (List<ModulePOCO>)Session["ddlModule"];
                    DashboardPOCO obj = new DashboardPOCO();
                    obj.Module_name = "5S Audit";
                    obj.Module_id = "2";
                    obj.detail = GetDashboardDetails(Convert.ToString(obj.Module_id), "Monthly", "Week", Convert.ToString(obj.Module_name));
                    list.Add(obj);

                    list1 = listModule.AsEnumerable().Where(c => ModuleIds.Contains(c.module_id)).Select(c => new DashboardPOCO
                    {
                        Module_id = Convert.ToString(c.module_id),
                        Module_name = c.module,
                        detail = GetDashboardDetails(Convert.ToString(c.module_id), "Monthly", "Week", Convert.ToString(c.module))
                    }).ToList();
                    foreach (DashboardPOCO ob in list1)
                    {
                        list.Add(ob);
                    }
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
            }
            return View(list);
        }

        public ActionResult RedirectOpenClose(string audit_id)
        {

            return RedirectToAction("CloseOpenTask", "CloseOpenTask", new { audit_id = audit_id });

        }

        [HttpGet]
        public JsonResult GetDashboardChart(int module_id)
        {
            List<DashboardChart> listobj = new List<DashboardChart>();
            listobj = objModel.getDashboardChart(module_id);
            return Json(listobj, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        public JsonResult GetAverageScore(string Module_id, string Module_name, string freq)
        {
            DashboardDetailPOCO obj = new DashboardDetailPOCO();
            obj = objModel.getDashboardDetail(Module_id, Module_name, freq);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }
        [HttpGet]
        public JsonResult GetRecentScore(string Module_id, string Module_name, string freq)
        {
            List<AuditTypePOCO> obj = new List<AuditTypePOCO>();
            obj = objModel.getAuditTypeScores(Module_id, freq, Module_name);
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult DashboardSchedule(string Module_id, string freq)
        {
            List<AuditTypePOCO> obj = new List<AuditTypePOCO>();
            obj = objModel.getAuditTypeScores(Module_id, freq, "Audit");

            return PartialView("DashboardSchedule",obj);
        }

        public PartialViewResult DashboardPending(string Module_id, string Module)
        {
            Session["DashboardModule_id"] = Module_id;
            List<DashboardPOCO> list = new List<DashboardPOCO>();
            DashboardDetailPOCO listDetails = GetDashboardDetails(Module_id, "Monthly", "Week", "Audit");
            DashboardPOCO obj = new DashboardPOCO();
            obj.detail = listDetails;
            obj.Module_id = Module_id;
            obj.Module_name = Module;
            list.Add(obj);

            return PartialView("DashboardPending", list);
        }
        
    }
}

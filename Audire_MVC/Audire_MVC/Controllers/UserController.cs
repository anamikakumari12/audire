﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Audire_POCO;
using Audire_EF;
using System.Text;
using Audire_MVC.Utilities;
using System.Web.Security;
using Audire_MVC.Models;
using System.Globalization;


namespace Audire_MVC.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        Audire_dbEntities datacontext = new Audire_dbEntities();
        MastersModel objModel = new MastersModel();
        #region Login

        public ActionResult Login()
        {
            try
            {
                var result = datacontext.tbl_Module_master.Where(m => m.status == 1).
                    AsEnumerable().Select(c => new ModulePOCO
                    {
                        module = c.module,
                        module_id = c.module_id
                    }).ToList();
                // tblUser.global_admin_flag = objUser.Admin == true ? "Y" : "N";

                //var result1 = datacontext.tbl_user_master.Where(m => Convert.ToBoolean(m.global_admin_flag == true ? "Y" : "N" )).
                //        AsEnumerable().Select(c => new UsersPOCO
                //        {
                //            Admin = c.global_admin_flag

                //        }).ToList();
                Session["ddlModule"] = result;
                return View();

            }
            catch (Exception ex)
            {
                ModelState.AddModelError("LogOnError", "Error has been Occured. Please Try Again Later..");
                objModel.ErrorLog(ex);
                return View();
            }

        }

        [HttpPost]
        public ActionResult Login(LoginPOCO objloginpoco)
        {
            try
            {
                if (!string.IsNullOrEmpty(objloginpoco.username) && !string.IsNullOrEmpty(objloginpoco.password))
                {
                    List<tbl_user_master> lstempdata = new List<tbl_user_master>();
                    tbl_user_master login_user = new tbl_user_master();
                    login_user.username = objloginpoco.username;
                    string MD5pwd = CommonFunctions.MD5Encrypt(objloginpoco.password);
                    string password = CommonFunctions.Passwordencryption(MD5pwd);
                    // string password = CommonFunctions.Passwordencryption(MD5pwd);
                    //String kns = CommonFunctions.Passwordencryption("kns");
                    // string decryptpwd = CommonFunctions.Decrypt(objloginpoco.password);
                    login_user.passwd = password;
                    lstempdata = objModel.ValidateUser(login_user);
                    if (lstempdata.Count == 0)
                    {
                        ModelState.AddModelError("LogOnError", "Username or password provided is invalid");
                    }

                    //var result = datacontext.tbl_user_master.
                    //Where(
                    //m => m.username == login_employee.username &&
                    //m.passwd == login_employee.passwd).
                    //AsEnumerable().ToList();

                    //lstempdata.AddRange(result);

                    //catch (Exception ex)
                    //{
                    //    return RedirectToAction("Role", "Role");
                    //    CommonFunctions.ErrorLog(ex);
                    //}
                    //if (lstempdata.Count == 0)
                    //{
                    //    return RedirectToAction("Role", "Role");
                    //    //ModelState.AddModelError("LogOnError", "Email address or password provided is invalid");
                    //}
                    else
                    {
                        foreach (var result in lstempdata)
                        {
                            int valid = 0;
                            if(result.end_date==null)
                            {
                                valid++;
                            }
                            else if(result.end_date.Value > DateTime.Now)
                            {
                                valid++;
                            }

                            if (valid>0)
                            {
                                var authTicket = new FormsAuthenticationTicket(
                                    1, result.emp_full_name, DateTime.Now,
                                    DateTime.Now.AddHours(2), false,
                                    "Admin",
                                    "/"
                                    );
                                Response.Cookies.Clear();
                                var cookie = new HttpCookie(FormsAuthentication.FormsCookieName, FormsAuthentication.Encrypt(authTicket));
                                Response.Cookies.Add(cookie);
                                HttpContext.Session["LoginFullname"] = result.emp_full_name;
                                HttpContext.Session["LoginUsername"] = result.username;
                                HttpContext.Session["EmailId"] = result.email_id;
                                HttpContext.Session["LoginID"] = result.user_id;
                                HttpContext.Session["Password"] = MD5pwd;
                                HttpContext.Session["global_admin"] = result.global_admin_flag;
                                HttpContext.Session["ModuleAccessed"] = result.module_id;

                                List<tbl_role_master> perObj = new List<tbl_role_master>();
                                var role_access = datacontext.tbl_role_master.Where(m => m.Role_id == result.role_id).AsEnumerable().ToList();
                                perObj.AddRange(role_access);
                                if (perObj.Count > 0)
                                {
                                    HttpContext.Session["Permissions"] = perObj[0].Role_Access; 
                                }

                                //DashboardPOCO ojdashboard = new DashboardPOCO();
                                //Session["global_admin"] = ojdashboard.Admin;

                                ddlLocationPOCO objLoginLoc = new ddlLocationPOCO();
                                objLoginLoc.region_id = Convert.ToInt32(result.region_id);
                                objLoginLoc.country_id = Convert.ToInt32(result.country_id);
                                objLoginLoc.location_id = Convert.ToInt32(result.location_id);
                                Session["objLoginLoc"] = objLoginLoc;


                                return RedirectToAction("Dashboard", "Dashboard");

                            }
                            else
                            {
                                ModelState.AddModelError("LogOnError", "Account has been Deactivated Please Contact your Administrator");
                            }
                        }

                    }
                }
                else
                {
                    ModelState.AddModelError("LogOnError", "please enter username and password.");
                }

                return View(objloginpoco);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError("LogOnError", ex.Message);
                objModel.ErrorLog(ex);
                return View();
            }

        }
        #endregion

        #region ForgotPassword
        public ContentResult ForgotPassword(string UserName)
        {
            tbl_user_master emp = objModel.GetLoginUserDetail(UserName);
            if (emp != null)
            {
                //   emp.username = UserName;
                //  emp.email_id = UserEmail;
                var password = CommonFunctions.CreateRandomPassword();
                string success = objModel.Sendmail(emp.email_id, password, emp.username, "forgotPassword");
                if (success == "Yes")
                {
                    emp.passwd = CommonFunctions.MD5Encrypt(password);
                    objModel.ResetPassword(emp);
                    return Content("Yes");
                }
                else
                {
                    return Content("No");
                }
            }
            else
            {
                return Content("Error");
            }

        }
        #endregion

        #region Users
        public ActionResult Users()
        {
            if (!string.IsNullOrEmpty(Convert.ToString(Session["LoginID"])) && (Convert.ToString(Session["Permissions"]).Split(',').Select(int.Parse).ToList().Contains(Convert.ToInt32(Resources.ConstantValues.Admin))))
            {
                List<UsersPOCO> listUsers = new List<UsersPOCO>();
                try
                {
                    Session["ddlLocation"] = objModel.getLocationList();
                    List<RolePOCO> listRole = objModel.getRoleList();
                    Session["listRole"] = listRole;
                    listUsers = objModel.getUsersList();
                    Session["listUsers"] = listUsers;
                    Session["ddlProcess"] = objModel.ProcessList();
                }
                catch (Exception ex)
                {
                    objModel.ErrorLog(ex);
                }
                return View(listUsers);

            }
            else
            {
                return RedirectToAction("Login", "User");
            }
        }

        public PartialViewResult EditUser(int user_id)
        {
            List<UsersPOCO> listUsers = new List<UsersPOCO>();
            listUsers = objModel.getUsersList();

            if (user_id == 0)
            {
                var user = (from s in listUsers.AsEnumerable()
                            select s).ToList();
                return PartialView("EditUser", user[0]);
            }
            else
            {
                var user = (from s in listUsers.AsEnumerable()
                            where s.user_id == user_id
                            select s).ToList();
                return PartialView("EditUser", user);
            }
        }

        public PartialViewResult DisplayUser(string Id, string viewName)
        {
            List<UsersPOCO> listUsers = new List<UsersPOCO>();
            listUsers = objModel.getUsersList();
            if (string.IsNullOrEmpty(Id) || Id == "0")
            {
                var user = (from s in listUsers.AsEnumerable()
                            select s
                            ).ToList();
                return PartialView("DisplayUser", user[0]);
            }
            else
            {
                var user = (from s in listUsers.AsEnumerable()
                            where s.user_id == Convert.ToInt32(Id.Trim())
                            select s).ToList();
                return PartialView(viewName, user);
            }

        }

        public PartialViewResult AddNewUser()
        {
            List<UsersPOCO> listUsers = new List<UsersPOCO>();
            UsersPOCO ObjUser = new UsersPOCO();
            listUsers.Add(ObjUser);
            return PartialView("EditUser", listUsers);
        }

        [HttpGet]
        public JsonResult IsUserExists(string UserName, string UserId)
        {
            int id = Convert.ToInt32(UserId);
            ErrorOutput objerr = new ErrorOutput();
            bool validateName = (datacontext.tbl_user_master.Any(x => x.username == UserName));
            if (validateName == true)
            {
                if (id > 0)
                {
                    List<UsersPOCO> listUser = new List<UsersPOCO>();

                    listUser = datacontext.tbl_user_master.Where(m => m.user_id == id).
                            AsEnumerable().OrderBy(m => m.username)
                            .Select(c => new UsersPOCO
                            {
                                user_id = c.user_id,
                                UserName = c.username
                            }).ToList();
                    if (!(listUser[0].UserName == UserName))
                    {
                        objerr.err_message = "Username already exist.";
                        objerr.err_code = 200;
                    }
                }
                else
                {
                    objerr.err_message = "Username already exist.";
                    objerr.err_code = 200;
                }
            }
            return Json(objerr, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult IsEmailIdExists(string EmailId, string UserId)
        {
            int id = Convert.ToInt32(UserId);
            ErrorOutput objerr = new ErrorOutput();
            bool validateName = (datacontext.tbl_user_master.Any(x => x.email_id == EmailId));
            if (validateName == true)
            {
                if (id > 0)
                {
                    List<UsersPOCO> listUser = new List<UsersPOCO>();

                    listUser = datacontext.tbl_user_master.Where(m => m.user_id == id).
                            AsEnumerable().OrderBy(m => m.username)
                            .Select(c => new UsersPOCO
                            {
                                user_id = c.user_id,
                                EmailId = c.email_id
                            }).ToList();
                    if (!(listUser[0].EmailId == EmailId))
                    {
                        objerr.err_message = "Email Id already exist.";
                        objerr.err_code = 200;
                    }
                }
                else
                {
                    objerr.err_message = "Email Id already exist.";
                    objerr.err_code = 200;
                }
            }
            return Json(objerr, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveUser(UsersPOCO m)
        {
            ErrorOutput obj = new ErrorOutput();
            UsersPOCO objUser = new UsersPOCO();
            objUser.UserName = Convert.ToString(m.UserName);
            objUser.FirstName = Convert.ToString(m.FirstName);
            objUser.LastName = Convert.ToString(m.LastName);
            objUser.EmailId = Convert.ToString(m.EmailId);
            //Convert.ToString(Request["m.EmailId"]);
            objUser.password = Convert.ToString(m.password);
            objUser.location = Convert.ToString(m.location);
            objUser.privacySetting = Convert.ToBoolean(m.privacySetting);
            //objUser.Admin = Convert.ToBoolean(m.Admin);
            //objUser.Admintype=Convert.ToString(Request.Form["Admin"]);
            objUser.Role = Convert.ToString(m.Role);
            objUser.module = Convert.ToString(m.module);
            objUser.module_id_list = Convert.ToString(m.module_list);
            objUser.process = Convert.ToString(m.process);
            objUser.reporting_manager = Convert.ToInt32(m.reporting_manager);
            string UIpattern = "dd-MM-yyyy";
            string DBPattern = "MM-dd-yyyy";
            DateTime parsedDate;
            if (!string.IsNullOrEmpty(Convert.ToString(m.startdate)))
            {
                if (DateTime.TryParseExact(Convert.ToString(m.startdate), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.ToString(DBPattern);
                    objUser.startdate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                }
                else
                {
                    objUser.startdate = Convert.ToDateTime(Convert.ToString(m.startdate));
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(m.enddate)))
            {

                if (DateTime.TryParseExact(Convert.ToString(m.enddate), UIpattern, null, DateTimeStyles.None, out parsedDate))
                {
                    string frmdate = parsedDate.ToString(DBPattern); // one extra day is adding for existing date. sql 
                    objUser.enddate = DateTime.ParseExact(frmdate, "MM-dd-yyyy", null);
                }
                else
                {
                    objUser.enddate = Convert.ToDateTime(Convert.ToString(m.enddate));
                }
            }
            // objUser.startdate = Convert.ToDateTime(Request["m.startdate"]);
            // objUser.enddate = Convert.ToDateTime(Request["m.enddate"]);
            if (m.password != null)
            {
                objUser.encryptedpwd = CommonFunctions.Passwordencryption(CommonFunctions.MD5Encrypt(m.password));
            }
            tbl_user_master tblUser = new tbl_user_master();
            if (Convert.ToInt32(m.user_id) == 0)
            {

                tblUser.country_id = objModel.getCountryId(location_id: Convert.ToInt32(objUser.location));
                tblUser.region_id = objModel.getRegionId(location_id: Convert.ToInt32(objUser.location));
                tblUser.location_id = Convert.ToInt32(objUser.location);
                tblUser.username = objUser.UserName;
                tblUser.passwd = objUser.encryptedpwd;
                tblUser.emp_full_name = objUser.FirstName + " " + objUser.LastName;
                tblUser.emp_first_name = objUser.FirstName;
                tblUser.emp_last_name = objUser.LastName;
                tblUser.display_name_flag = objUser.privacySetting == true ? "Y" : "N";
                tblUser.email_id = objUser.EmailId;
                tblUser.role = objModel.getRole(role_id: Convert.ToInt32(objUser.Role));
                //tblUser.global_admin_flag = objUser.Admin == true ? "Y" : "N";
                //tblUser.site_admin_flag = objUser.Admintype == "Local" ? "Y" : "";
                //tblUser.Admin_Type = objUser.Admintype;
                tblUser.module_id = objUser.module;
                // tblUser.module_id = objUser.module_id_list;
                tblUser.process_id = objUser.process;
                objUser.process = Convert.ToString(m.process);
                objUser.process_id_list = Convert.ToString(m.process_list);
                tblUser.start_date = objUser.startdate;
                tblUser.end_date = objUser.enddate;
                tblUser.role_id = Convert.ToInt32(objUser.Role);
                tblUser.reporting_manager = Convert.ToInt32(objUser.reporting_manager);
                datacontext.tbl_user_master.Add(tblUser);
                datacontext.SaveChanges();
                obj.err_code = 200;
                obj.err_message = "User details are saved successfully.";
                TempData["usermessage"] = "User details are saved successfully.";
            }
            else
            {
                tblUser = datacontext.tbl_user_master.AsQueryable().
                    FirstOrDefault(k => k.user_id == (int)m.user_id);
                if (tblUser != null)
                {
                    tblUser.country_id = objModel.getCountryId(location_id: Convert.ToInt32(objUser.location));
                    tblUser.region_id = objModel.getRegionId(location_id: Convert.ToInt32(objUser.location));
                    tblUser.location_id = Convert.ToInt32(objUser.location);
                    tblUser.username = objUser.UserName;
                    // tblUser.passwd = objUser.encryptedpwd;
                    tblUser.emp_full_name = objUser.FirstName + " " + objUser.LastName;
                    tblUser.emp_first_name = objUser.FirstName;
                    tblUser.emp_last_name = objUser.LastName;
                    tblUser.display_name_flag = objUser.privacySetting == true ? "Y" : "N";
                    tblUser.email_id = objUser.EmailId;
                    tblUser.role = objModel.getRole(role_id: Convert.ToInt32(objUser.Role));
                    //tblUser.global_admin_flag = objUser.Admin == true ? "Y" : "N";
                    //tblUser.site_admin_flag = objUser.Admintype == "Local" ? "Y" : "";
                    //tblUser.Admin_Type = objUser.Admintype;
                    tblUser.module_id = objUser.module;
                    // tblUser.module_id = objUser.module_id_list;
                    tblUser.process_id = objUser.process;
                    tblUser.start_date = objUser.startdate;
                    tblUser.end_date = objUser.enddate;
                    tblUser.role_id = Convert.ToInt32(objUser.Role);
                    tblUser.reporting_manager = Convert.ToInt32(objUser.reporting_manager);
                    datacontext.SaveChanges();
                    obj.err_code = 200;
                    obj.err_message = "User details are updated successfully.";
                    TempData["usermessage"] = "User details are updated successfully.";
                }
                else
                {
                    obj.err_code = 200;
                    obj.err_message = "No User is found for update. Please try again.";
                    TempData["usermessage"] = "No User is found for update. Please try again.";
                }
            }
            return Json(obj, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Users", "User");
        }

        [HttpPost]
        public JsonResult SetTempData(string usermessage)
        {
            // Set your TempData key to the value passed in
            TempData["usermessage"] = usermessage;
            return Json(true);
        }
        [HttpGet]
        public JsonResult GetManager(string location)
        {
            List<SelectListItem> list = new List<SelectListItem>();
            List<UsersPOCO> listManager = new List<UsersPOCO>();
            try
            {
                listManager = (List<UsersPOCO>)Session["listUsers"];

                list = listManager.AsEnumerable()
                    .Where(c => c.location_id == Convert.ToInt32(location))
                    .Select(c => new SelectListItem
                    {
                        Value = Convert.ToString(c.user_id),
                        Text = c.FullName
                    }).ToList();
                return Json(list.Distinct().ToList(),JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                CommonFunctions.ErrorLog(ex);
                return Json(list,JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region LogOut
        public ActionResult LogOut()
        {
            try
            {
                HttpCookie c = Request.Cookies[FormsAuthentication.FormsCookieName];
                c.Expires = DateTime.Now.AddDays(-1);
                // Update the amended cookie!
                Response.Cookies.Set(c);
                Session.Clear();
                FormsAuthentication.SignOut();
                TempData["loginmessage"] = "You have successfully Logged out.";
                return RedirectToAction("Login", "User");
            }
            catch (Exception)
            {
                TempData["loginmessage"] = "Error has been occured please LOGIN again";
                return RedirectToAction("Login", "User");
            }

        }
        #endregion

        #region Reset Password
        [HttpPost]
        public JsonResult ResetPassword(string oldPassword, string newPassword)
        {
            OutputPOCO objOutput = new OutputPOCO();
            string oldpwd = CommonFunctions.MD5Encrypt(oldPassword);
            string newpwd = CommonFunctions.MD5Encrypt(newPassword);
            tbl_user_master emp = new tbl_user_master();
            if (Convert.ToString(Session["Password"]) == oldpwd)
            {
                emp.username = Convert.ToString(Session["LoginUsername"]);
                emp.email_id = Convert.ToString(Session["EmailId"]);
                emp.user_id = Convert.ToInt32(Session["LoginID"]);
                emp.passwd = newpwd;
                objModel.ResetPassword(emp);
                string success = objModel.Sendmail(emp.email_id, newPassword, emp.username, "resetPassword");
                if (success == "Yes")
                {
                    Session["Password"] = newpwd;
                    objOutput.err_code = 200;
                    objOutput.err_msg = "Password is updated successfully.";
                    return Json(objOutput, JsonRequestBehavior.AllowGet);
                    //return Content("Password is updated successfully.");
                }
                else
                {
                    objOutput.err_code = 250;
                    objOutput.err_msg = "There is issue in updating password. Please try again.";
                    return Json(objOutput, JsonRequestBehavior.AllowGet);
                    //return Content("There is issue in updating password. Please try again.");
                }
            }
            else
            {
                objOutput.err_code = 250;
                objOutput.err_msg = "Old password is invalid.";
                return Json(objOutput, JsonRequestBehavior.AllowGet);
                //return Content("Old password is not correct.", "text/plain", Encoding.UTF8);
            }

        }

        #endregion
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Audire_POCO
{
    public class RolePOCO
    {
        public int role_id { get; set; }
        public string role_name { get; set; }
        public string users { get; set; }
        public string permissions { get; set; }
        public HtmlString htmlusers { get; set; }
        public List<Permissions> permission { get; set; }
    }

    public class Permissions
    {
        public int ID { get; set; }
        public string Permission { get; set; }

    }

    public class ProcesssPOCO
    {
        public int id { get; set; }
        public string Process { get; set; }
        public string user_id { get; set; }
        public string emp_full_name { get; set; }
        public int? module_id { get; set; }
        public string module { get; set; }
    }
    public class LocationPOCO
    {
        public int region_id { get; set; }
        public string region { get; set; }
        public int country_id { get; set; }
        public string country { get; set; }
        public int location_id { get; set; }
        public string location { get; set; }
        public int shifts { get; set; }
        public string timezone { get; set; }
        public TimeSpan starttime1 { get; set; }
        public TimeSpan starttime2 { get; set; }
        public TimeSpan starttime3 { get; set; }
        public TimeSpan endtime1 { get; set; }
        public TimeSpan endtime2 { get; set; }
        public TimeSpan endtime3 { get; set; }
        public string TimeZoneId { get; set; }
        public TimeZoneInfo TimeZone
        {
            get { return TimeZoneInfo.FindSystemTimeZoneById(TimeZoneId); }
            set { TimeZoneId = value.Id; }
        }
    }

    public class UsersPOCO
    {
        public int user_id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string EmailId { get; set; }
        public int? location_id { get; set; }
        public string location { get; set; }
        public string Role { get; set; }
        public Boolean Admin { get; set; }
        public DateTime? startdate { get; set; }
        public DateTime? enddate { get; set; }
        public Boolean privacySetting { get; set; }
        public string password { get; set; }
        public string encryptedpwd { get; set; }
        //public string Module_id { get; set; }
        public string FullName { get; set; }
        public string module_id { get; set; }
        public string module { get; set; }
        public string id { get; set; }
        public string process { get; set; }
        public string module_list { get; set; }
        public string module_id_list { get; set; }
        public string process_list { get; set; }
        public string process_id_list { get; set; }
        public int reporting_manager { get; set; }
    }

    public class LinePOCO
    {
        public int line_id { get; set; }
        public int location_id { get; set; }
        public string location_name { get; set; }
        public int country_id { get; set; }
        public int region_id { get; set; }
        public string region { get; set; }
        public string line_code { get; set; }
        public string line_name { get; set; }
        public DateTime? start_date { get; set; }
        public DateTime? end_date { get; set; }
        public string distribution_list { get; set; }
        public string country { get; set; }
        public string location { get; set; }
        public string Module_id { get; set; }
    }

    public class RegionPOCO
    {
        public int region_id { get; set; }
        public string region_name { get; set; }
    }

    public class CountryPOCO
    {
        public int country_id { get; set; }
        public string country_name { get; set; }
    }

    public class PartPOCO
    {
        public int line_id { get; set; }
        public int location_id { get; set; }
        public string location_name { get; set; }
        public int country_id { get; set; }
        public int region_id { get; set; }
        public string region { get; set; }
        public string line_code { get; set; }
        public string line_name { get; set; }
        public int product_id { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public string country { get; set; }
        public string location { get; set; }
    }


    public class BulkQRPOCO
    {
        public int line_id { get; set; }
        public int location_id { get; set; }
        public string location_name { get; set; }
        public int country_id { get; set; }
        public int region_id { get; set; }
        public string region { get; set; }
        public string line_code { get; set; }
        public string line_name { get; set; }
        public int product_id { get; set; }
        public string product_code { get; set; }
        public string product_name { get; set; }
        public string country { get; set; }
        public string location { get; set; }

    }

    public class QuestionsPOCO
    {
        public int question_id { get; set; }
        public int section_id { get; set; }
        public string section_name { get; set; }
        public string question { get; set; }
        public string Audit_type_id { get; set; }
        public int ID { get; set; }
        public string module_id { get; set; }
        public string module { get; set; }
        public string  AuditType { get; set; }
    }


    public class PublicHoliday
    {
        public int Sr { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Start_Date { get; set; }
        public string End_Date { get; set; }
    }

    public class PerformAuditPOCO
    {
        public int question_id { get; set; }
        public string question { get; set; }
        public int id { get; set; }
        public string process { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string filepath { get; set; }
        public string audit_type { get; set; }
        public string help_text { get; set; }
        public string question_counter { get; set; }
        public int ID { get; set; }
        public string answer { get; set; }
        public string assignedTo { get; set; }
        public string comment { get; set; }

        public string auditor_comment { get; set; }
        public int audit_id { get; set; }
        public string filename { get; set; }
    }

    public class OpenCloseTaskPOCO
    {
        public int answer_id { get; set; }
        public int audit_id { get; set; }
        public int question_id { get; set; }     
        public string answer { get; set; }
        public string audit_comment { get; set; }
        public string audit_images { get; set; }
        public string question { get; set; }
        public string answer_counter { get; set; }
        public string review_comment { get; set; }
        public string module_id { get; set; }
        public string audit_number { get; set; }
        public string root_cause { get; set; }
        public string corrective_action { get; set; }
        public string preventive_action { get; set; }
        public string assigned_to { get; set; }
        public string clause { get; set; }
    }

    public class OutputPOCO
    {
        public int audit_type_id { get; set; }
        public int err_code { get; set; }
        public string err_msg { get; set; }
    }

    public class ddlOpenTaskPOCO
    {
        public int? region_id { get; set; }
        public int? country_id { get; set; }
        public int? location_id { get; set; }
        public int? module_id { get; set; }
        public int? audit_type { get; set; }
    }

    public class DashboardGraph
    {
        public DateTime x { get; set; }
        public int y { get; set; }
    }
}

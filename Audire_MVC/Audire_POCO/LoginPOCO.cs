﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Audire_POCO
{
    public class ddlLocationPOCO
    {
        public int region_id { get; set; }
        public int country_id { get; set; }
        public int location_id { get; set; }
    }
    public class LoginPOCO
    {
        public string username { get; set; }
        public string password { get; set; }
    }
    public class ModulePOCO {
        public string module { get; set; }
        public int module_id { get; set; }
        public int status { get; set; }
    }

    public class AuditResult {
        public string audit_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string userID { get; set; }
        public string deviceID { get; set; }
        public string module_id { get; set; }
        public string region_id { get; set; }
        public string country_id { get; set; }
        public string location_id { get; set; }
        public string line_id { get; set; }
        public string product_id { get; set; }
        //public string shift_no { get; set; }
        public int auditType_id { get; set; }
        public string part { get; set; }
        public List<QuestionAnswer> questionAnswer { get; set; }
        public List<reviewStatus> reviewStatus { get; set; }
        public string Auditor_comment { get; set; }
        public string IsCompleted { get; set; }
    }

    public class QuestionAnswerWithComment
    {
        public int audit_id { get; set; }
        public string IsCompleted { get; set; }
        public string Auditor_comment { get; set; }
        public List<QuestionAnswer> questionAnswer { get; set; }
    }
    public class QuestionsImage
    {
        public string image { get; set; }
        public int question_counter { get; set; }
    }
    public class AuditResultOutput
    {
        public int err_code { get; set; }
        public string err_message { get; set; }
        public int audit_id { get; set; }
        public string audit_report { get; set; }
    }
    public class QuestionAnswer
    {
        public string question_id { get; set; }
        public string Answer { get; set; }
        public string comment { get; set; }
        public string assignedTo { get; set; }
        public string img_content { get; set; }
    }
    public class reviewStatus
    {
        public string answer_id { get; set; }
        public string review_closed_on { get; set; }
        public string review_closed_status { get; set; }
        public string review_comments { get; set; }
        public string review_image_file_name { get; set; }
        public string root_cause { get; set; }
        public string CA { get; set; }
        public string PA { get; set; }
    }
    public class ReviewImage
    {
        public string image { get; set; }
        public int answer_counter { get; set; }
    }
    public class ErrorOutput
    {
        public int err_code { get; set; }
        public string err_message { get; set; }
    }

    public class csSendReport
    {
        public string audit_id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string userID { get; set; }
        public string deviceID { get; set; }
    }

    public class BulkReview
    {
        public string userID { get; set; }
        public string deviceID { get; set; }
        public List<BulkReviewList> reviewObj { get; set; }
    }
    public class BulkReviewList
    {

        public int audit_id { get; set; }
        public int question_id { get; set; }
        public string review_closed_on { get; set; }
        public int review_closed_status { get; set; }
        public string review_comments { get; set; }
        public string review_image_file_name { get; set; }
    }
}
